# - *- coding: utf- 8 - *-
import sys
sys.path.append('/home/coollooter')
import fitz
import os
import json
import re
import metadata_extractor.module.helpers as h
import metadata_extractor.module.xml_extraction as xml
import metadata_extractor.module.subtitles as subs
import metadata_extractor.module.deleter as delete
import metadata_extractor.module.grouper as group
import metadata_extractor.module.sectioner as section
import metadata_extractor.module.tester as test
import metadata_extractor.grobid_extraction as grobid
import metadata_extractor.module.splitter as splitter
import metadata_extractor.module.rescaler as rescaler
import metadata_extractor.module.checker as check
from pymongo import MongoClient
import collections
from copy import deepcopy
import pypandoc
import pandas as pd
from sklearn.cluster import DBSCAN
from sklearn import metrics
import numpy as np
import matplotlib.pyplot as plt
'''
Section Extraction using Metadata

    This extractor focuses on using metadata as the main feature. We get metadata from a python library
    called PyMuPDF (fitz). The input of the algorithm is a pdf file and a list of common words that should
    be ignored ( usually these words are located in the headers ). This is the controller file that gives the
    linear structure of the algorithm.
'''

# TODO: correctly output the type of text and subsections
# TODO: support html along with subscripts and superscripts and unicode characters
# TODO: move abstract outside of contents
def extract(pdf, ignore_list, subsections=False, output='.json'):
    doc = fitz.Document(pdf)
    #print(doc)
    document_xml = xml.make_pages(doc)
    #print(document_xml)# -> TEST01_xml.xml
    page_list = xml.make_json(document_xml)
    #print(page_list)# -> TEST01_json.json
    line_list = make_lines(page_list)
    #print(line_list) # ->TEST01_line_list
    #if not test.detect_unrendered(line_list):

    span_list=[]
    daughter_span_list=[] # new span_list with only daughter spans. In daughter span, text value is not included
    span_index_dict={}    # span_index_dict[mother_span_index] = child_span_index_list 
    for i in range(len(line_list)):
        for j in range(len(line_list[i])):
            span_list.append(line_list[i][j])
    # indexing span in span_list before splitting a span into many points. 
    for i in range(len(span_list)): 
        span_list[i]['index']=i+1  # now mother span has its identical number indexing from 1. 
        #print(span_list[i])

    #Splitting a span block into many points.  
    daughter_index_start = 0 
    ind =daughter_index_start 
    for i in range(len(span_list)):
        #print(span_list[i]) # mother spas index 11 or 14
        mother_span= span_list[i] 
        #print(mother_span)
        daughter_spans, index_list = splitter.generating_point_span(mother_span,ind)
        span_index_dict[i+1]=index_list # match mother's index and daughter's index list
        #print(daughter_spans)
        ind = index_list[-1]
        daughter_span_list += daughter_spans
            
        # - span_index_dict  : matching table between mother span and daughter span(points)
        # - span_list : only has mother span information(original span list + indexing)
        # - daughter_span_list : only has all daughter spans(points) without context information 
    #for i in range(len(daughter_span_list)):
    #    print(daughter_span_list[i])

    mother_span_list=span_list
    rescaled_span= rescaling_span(daughter_span_list) # rescaled + panda dataframe
    print('clustering processing....')
    clustered_dspan= DBSCAN_operation(rescaled_span) # Clustered daughter points span using DBSCAN
    #print('post_processing....')
    mspan=post_processing(mother_span_list, clustered_dspan, span_index_dict)  # currently only return mother_span+cluster
    grouped_span=sort_for_grouping(mspan) # just for plugging into METASC algorithm, it is grouped .
    final_list=[];first_span=True;
    for page in grouped_span:
        page_list=[]
        for grouped in page:
            group_list=[]
            for span in grouped:
                if first_span:
                    group_list.append(span)
                    first_span=False
                else:
                    boolean, t =h.check_same_line(group_list[-1],span)
                    if boolean and not h.check_inline_subs(group_list[-1],span):
                        group_list[-1]=group.merge(group_list[-1],span,t)
                    else:
                        group_list.append(span)
            first_span=True
            page_list.append(group_list)
        final_list.append(page_list)
    grouped1=final_list
    grouped = delete.delete_figures(grouped1) # already grouped span
    subs_list = subs.extract_subtitles(grouped)
    #print(subs_list) # TEST01_subs_list
    final_subs = subs.clean_subs(subs_list)
    #print(final_subs) # TEST01_final_subs
    dynamic_list2 = group.recalculate_groups(grouped)
    #for i in range(len(dynamic_list2[0])):
    #    print(dynamic_list2[0][i]) # TEST01_dynamic_list2
    sorted_list = group.group_into_columns(dynamic_list2, final_subs)
    sorted_list = delete.delete_headers_footers(sorted_list)
    final_subs2 = subs.re_get_subtitles(sorted_list, final_subs)
    #print(sorted_list) # TEST01_final_sorted_list
    #print(final_sub2) # TEST01_final_sub2222
    extracted = section.section_extraction(sorted_list, final_subs2)

    if not subsections:
        if output == '.json':
            return extracted
    else:
        for k in extracted['contents']:
            if k != 'abstract':
                subtitles = subs.extract_section_subtitles(extracted['contents'][k])
                subtitles = subs.clean_section_subs(subtitles)
                extracted['contents'][k] = subs.order_sub(extracted['contents'][k])
                new_section = section.sub_section_extraction(extracted['contents'][k], subtitles)
                extracted['contents'][k] = new_section
        if output == '.json':
            t = ""
            product = {'title': "", 'authors': "", "contents": deepcopy(extracted['contents'])}
            for line in extracted['title']['lines']:
                t = h.add_string(t, line['text'])
            product['title'] = t
            t = ""
            for line in extracted['authors']['lines']:
                t = h.add_string(t, line['text'])
            product['authors'] = t
            for k in extracted["contents"]:
                t = ""
                #if str(k) == 'abstract':
                if str(k.encode("utf-8")) == 'abstract': # LKS modi
                    for g in extracted['contents'][k]:
                        for line in g['lines']:
                            t = h.add_string(t, line['text'])
                    product["contents"][k] = t
                else:
                    for g in extracted['contents'][k]['free']:
                        for line in g['lines']:
                            t = h.add_string(t, line['text'])
                    product['contents'][k]["free"] = t
                    product['contents'][k]['subsections'] = []
                    for s in extracted['contents'][k]['subsections']:
                        new_s = []
                        t = ""
                        for line in s[0]['lines']:
                            t = h.add_string(t, line['text'])
                        new_s.append(t)
                        t = ""
                        for g in s[1]:
                            for l in g['lines']:
                                t = h.add_string(t, l['text'])
                        new_s.append(t)
                        product['contents'][k]['subsections'].append(new_s)
            return product
        elif output == ".html":
            html_string = "<!DOCTYPE html>\n<html>\n<head>\n<style>\nsup {\nvertical-align: super;\nfont-size: small;\n}\n</style>\n</head>\n<body>"
            t = ""
            product = {'title': "", 'authors': "", "contents": deepcopy(extracted['contents'])}
            for line in extracted['title']['lines']:
                if "html" in line.keys():
                    t = h.add_string(t, line['html'])
                else:
                    t = h.add_string(t, line['text'])
            #html_string += "\n<h1>\n{}\n</h1>".format(t)
            html_string += "\n<h1>\n{}\n</h1>".format(t.encode("utf-8")) # LKS modi
            t = ""
            for line in extracted['authors']['lines']:
                if "html" in line.keys():
                    t = h.add_string(t, line['html'])
                else:
                    t = h.add_string(t, line['text'])
            #html_string += "\n<p>\n{}\n</p>".format(t)
            html_string += "\n<p>\n{}\n</p>".format(t.encode("utf-8")) # LKS modi
            for k in extracted["contents"]:
                t = ""
                #if str(k) == 'abstract':
                if str(k.encode("utf-8")) == 'abstract': # LKS modi
                    html_string += "\n<h2>Abstract</h2>"
                    for g in extracted['contents'][k]:
                        for line in g['lines']:
                            if "html" in line.keys():
                                t = h.add_string(t, line['html'])
                            else:
                                t = h.add_string(t, line['text'])
                    #html_string += "\n<p>\n{}\n</p>".format(t)
                    html_string += "\n<p>\n{}\n</p>".format(t.encode("utf-8")) # LKS modi
                else:
                    #html_string += "\n<h2>{}</h2>".format(k)
                    html_string += "\n<h2>{}</h2>".format(k.encode("utf-8")) # LKS modi
                    t = ""
                    for g in extracted['contents'][k]['free']:
                        for line in g['lines']:
                            if "html" in line.keys():
                                t = h.add_string(t, line['html'])
                            else:
                                t = h.add_string(t, line['text'])
                    #html_string += "\n<p>\n{}\n</p>".format(t)
                    html_string += "\n<p>\n{}\n</p>".format(t.encode("utf-8")) # LKS modi
                    product['contents'][k]['subsections'] = []
                    for s in extracted['contents'][k]['subsections']:

                        t = ""
                        for line in s[0]['lines']:
                            if "html" in line.keys():
                                t = h.add_string(t, line['html'])
                            else:
                                t = h.add_string(t, line['text'])
                        #html_string += "\n<h4>{}</h4>".format(t)
                        html_string += "\n<h4>{}</h4>".format(t.encode("utf-8")) # LKS modi
                        t = ""
                        for g in s[1]:

                            for l in g['lines']:
                                if "html" in l.keys():
                                    t = h.add_string(t, l['html'])
                                else:
                                    t = h.add_string(t, l['text'])
                        #html_string += "\n<p>\n{}\n</p>".format(t)
                        html_string += "\n<p>\n{}\n</p>".format(t.encode("utf-8")) # LKS modi
            html_string += "\n</body>\n</html>"
            return html_string
        else:
            product = extracted
        return product



def handle_output_type(extracted, output):
    if output == 'json':
        return extracted
    elif output == 'json_s':
        return extracted
    elif output == 'text':
        final_string = ""
        for line in extracted['title']['lines']:
            final_string += line['text'] + ' '
        final_string += '\n\n'

        return extracted
    else:
        return "option not supported"


def print_all(dynamic_list):
    """
    This is a function for debuging purposes. It loops through a document list and prints in an orderly manner

    :param dynamic_list: list to be printed
group   :type dynamic_list: list(str)
    """
    for page in dynamic_list:
        #print("PAGE:")
        a=1
        for group in page:
            # print("\tGROUP:")
            #print("\t\t" + str(group['column']) + "\t" + str(group))
            #print("\t\t" + str(group)) # LKS modi this line
            # for span in group['lines']:
            #     print("\t\t" + str(span))
            a=1

def print_subs(sub_list):
    """
    This is a function for debuging purposes. It loops through a subtitle list and prints it

    :param sub_list: list to be printed
    :type sub_list: list(str)
    """
    for sub in sub_list:
        print(sub)


def make_lines(document_json):
    """
    Function that takes the output of PyMuPDF and extracts all spans in order to discard the order
    in which the library extracted it as. Returns a list of pages, each page is a list of spans

    :param document_json: output from PyMuPDF, dictionary
    :type document_json: dict
    :return: list(list(dict))
    """
    document_list = []
    for i, page in enumerate(document_json):
        page_list = []
        for blocks in page["blocks"]:#blocks means page here
            for line in blocks["lines"]:
                for span in line["spans"]:
                    span['page'] = i + 1
                    page_list.append(span)
        document_list.append(page_list)
    '''
    document_list = []
    for page in document_json:
        page_list = []
        for blocks in page["blocks"]:
            for line in blocks["lines"]:
                page_list.append(line)
        document_list.append(page_list)
        '''
    return document_list


def file_list(root, recursive=True, file_types=['.pdf']):
    """
    Given a path to a directory, return a list of absolute paths to all
    of the files found within.

    :param root: An absolute or relative path to a directory
    :type root: str
    :param recursive: Whether to search the given directory recursively
    :type recursive: bool
    :return: A list of absolute paths to all files contained within
        the given directory and all of its subdirectories
    :rtype: list(str)
    """
    if os.path.isfile(root):
        return [os.path.abspath(root)]

    fpaths = []

    for abs_path, dirs, fnames in os.walk(root):
        abs_path = os.path.abspath(abs_path)
        fpaths.extend(map(lambda x: os.path.join(abs_path, x),
                          filter(lambda name: os.path.splitext(name)[1] in file_types, fnames)))

        if not recursive:
            break

    return fpaths


def send_to_json(references, json_path):
    with open(json_path, 'r') as json_file:
        data = json.load(json_file)
        data['raw_text']['extracted_sections']['references'] = references

    with open(json_path, 'r') as json_file:
        data = json.load(json_file)
        data['multiline_word_fix_text']['extracted_sections']['references'] = references
    with open(json_path, 'w') as json_file:
        json_file.write(json.dumps(data))


# TODO: call extract() rather than clean()
# TODO: ask team what is the best way to keep new subsections on mongo and show them on fingolfin

def run_DBSCAN():
    # This function follow below procedure 
    # -step1. extract metadata from pdf file 
    # -step2. gather all span list, build points list and its index lookup table
    # -step3. plug points list into DBSCAN algorithm for grouping
    products = []
    #file_number_list=[1,3,8,9,11,12,13,14,17,19,20,23,25,26,28]
    file_number_list=range(0,300)
  
    for i in file_number_list:
        if os.path.exists("/home/coollooter/Carlos_document/PDF/RELEVANT/{}.pdf".format(i)) == False:
            continue
        if os.path.exists("/home/coollooter/Carlos_document/JSON/RELEVANT/BASE/{}.json".format(i))==False:
            continue
        print ("file number is:", i)
        product = extract("/home/coollooter/Carlos_document/PDF/RELEVANT/{}.pdf".format(i),
                            ["copyright", "journal", "publisher", "downloaded", "licensed", "download",
                            #"doi", "ip", '■', 'XXX', 'org/', 'org'], subsections=True, output='.html')
                            "doi", "ip", '■', 'XXX', 'org/', 'org'], subsections=True, output='.json') # LKD modi line 500
        with open('/home/coollooter/metadata_extractor/Final_output/DB_META/%s.json'%(i),"w")as fp:
            json.dump(product,fp)
    return None


def rescaling_span(daughter_span_list):
    # This function put different weight on each features to help later algorithm learn correctly.
    # daughter_span_list has below information and characteristic 
    """
    x_val: x coordinate, y_val: y coordinate ==> These value has major effect on clustering
    font: font type ==> This value has less effect  than coordinate (might be equation, or special character in the middle in the line)
    size: font size ==> Smaller size has less penalty but bigger size has more penalty so it woudld be grouped in different cluster
    index: index ==> This is used for post processing
    page: It has enormous panelty on measuring distance each page is in different cluster
    """
    pd_span_list = rescaler.convert(daughter_span_list) # convert span list to panda dataframe  
    pd_span_list = rescaler.page_scale(pd_span_list,200) # multiply 200 for page column 
    pd_span_list = rescaler.size_scale(pd_span_list,0.05,10) # rescale font size with below step, above step
    pd_span_list = rescaler.x_coordi_scale(pd_span_list,1) # rescale x_coordinate with scale ratio ex) ratio=0.5

    pd_span_list = pd.get_dummies(pd_span_list,prefix=['font'],drop_first=True) # hot encoding for font type
    pd_span_list = rescaler.type_scale(pd_span_list,0.1) # rescale hot-encoded dummy value 
    #TODO: later, we might need scaling font type difference.
    #print(pd_span_list)
    return pd_span_list


def DBSCAN_operation(dataframe):
    #TODO: plug all column except 'index' column  
    #print(rescaled_span)
    index_df =dataframe['index'] # extract index dataframe for later merge
    #print(index_df)
    dataframe= dataframe.drop(columns=['index'])
    db = DBSCAN(eps=0.5,min_samples=1).fit(dataframe) #  User-defined EPS Value
    core_sample_mask =np.zeros_like(db.labels_,dtype=bool)
    core_sample_mask[db.core_sample_indices_]=True
    labels =db.labels_
    #print(labels)
    print("# of daughter span points:",len(labels))
    n_clusters_ = len(set(labels)) -( 1 if -1 in labels else 0 )
    n_noise_ = list(labels).count(-1)
    print ("the number of cluster :", n_clusters_)
    print ("the number of noise points :", n_noise_)
    # Merge original dataframe with cluster number 
    cluster_df = pd.DataFrame({'cluster':labels})
    merged_df = pd.concat([index_df, dataframe, cluster_df],axis=1)  # ==> original data frame with index number and cluster number
    #print(merged_df) #
    return merged_df 

    # plot result
def post_processing(mother_span, clustered_dspan, index_table): 
    #print(mother_span)
    #print(index_table)
    #print(clustered_dspan)
    
    # check if one unit mother span has different cluster by daughter span when DBSCAN works.
    check.if_multispan(mother_span,clustered_dspan,index_table) # if this result show caution!, which means eps can be very small
    for i in range(len(mother_span)): # mother_span index number = i+1 
        dspan_list =index_table[i+1]; # index table searched by real index number  
        mspan_cluster_num = clustered_dspan.iloc[dspan_list[0]-1]['cluster'] # cluster of 'a' mother span ; find by first dspan cluster number assumming all the dspan cluster number would be the same in the same mother span group ( This check is done in 'if_multispan' method)
        mother_span[i]['cluster'] = int(mspan_cluster_num) # applying cluster number to the mother span
    #print(mother_span) 
    # plotting using x,y coordinate to show clustered 
    # only for 1st page
    #df = clustered_dspan[clustered_dspan['page'] ==200] # fist page extraction
    #df.to_csv('./clustered_dspan.csv') # save file 
    #df.plot(kind='scatter', x='x_val',y='y_val', c='cluster',figsize=(50,100),colormap='viridis')
    #plt.savefig('test.pdf')
    return mother_span

def sort_for_grouping(mother_span):
    page_list= [x['page'] for x in mother_span]; page_list=remove_duplicate(page_list);
    page_num= len(page_list); sorted_p = [[]] * page_num; #print(sorted_p)
    for span in mother_span:
        span_page_num=span['page'] 
        if len(sorted_p[span_page_num-1]) ==0:
            temp=[]; sorted_p[span_page_num-1]=temp; sorted_p[span_page_num-1].append(span)
        else:
            sorted_p[span_page_num-1].append(span)
    #print(sorted_p)
    final_list=[]
    for page in sorted_p:
        cluster_list=[x['cluster'] for x in page]; cluster_list=remove_duplicate(cluster_list);
        cluster_count=len(cluster_list); sorted_c=[[]]*cluster_count;
        for span in page:
            span_cluster_num=span['cluster']; index=cluster_list.index(span_cluster_num)
            if len(sorted_c[index])==0:
                temp=[]; sorted_c[index]=temp; sorted_c[index].append(span)
            else:
                sorted_c[index].append(span)
        final_list.append(sorted_c)
    return final_list

def remove_duplicate(alist):
    res=[]
    for i in alist:
        if i not in res:
            res.append(i)
    return res
        
run_DBSCAN() # point span list

