# - *- coding: utf- 8 - *-
from metadata_extractor.content_extraction import debug, file_list
from pandas import DataFrame
import numpy as np
from sklearn.cluster import DBSCAN
from sklearn.preprocessing import StandardScaler
import pandas as pd
import os
from sklearn import metrics
import matplotlib.pyplot as plt

files = file_list("/Users/caguirre97/Downloads/19papers")
print(files)
print('hellow')
for f in files:
    product = extract(f, ["copyright", "journal", "publisher", "downloaded", "licensed", "download","doi", "ip", '�', 'XXX', 'org/', 'org'], subsections=True, output='.html')
    print(f[:-3]+"docx")
    pypandoc.convert(product, format="html", to="docx", outputfile=f[:-3]+"docx", extra_args=['-RTS'])






if __name__ == "__main__":

    papers = debug()

    for p in papers:
        p = [item for sublist in p for item in sublist]
        fonts = {}
        counter = 1
        data = []
        data_t = []
        for s in p:
            if s['font'] not in fonts:
                fonts[s['font']] = counter
                counter += 1
                # fonts[s['font']], s['size'],
            point = [fonts[s['font']], s['size'], s['bbox'][0], s['bbox'][1], s['page']]
            point_t = s['text']
            data.append(point)
            data_t.append(point_t)
        # data = np.array(data)
        print(data)
        # df = DataFrame(data, columns=['font', 'size', 'x0', 'y0', 'x1', 'y1', 'page']).T.plot()
        # plt.show()
        data = StandardScaler().fit_transform(data)
        print(data)
        # df.drop(["text"], axis=1, inplace=True)

        db = DBSCAN(eps=.1, min_samples=1).fit(data)
        core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
        core_samples_mask[db.core_sample_indices_] = True
        labels = db.labels_

        # Number of clusters in labels, ignoring noise if present.
        n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
        n_noise_ = list(labels).count(-1)

        print('Estimated number of clusters: %d' % n_clusters_)
        print('Estimated number of noise points: %d' % n_noise_)

        prev = 0
        for i, point in enumerate(data):
            if labels[i] != prev:
                print("\nNEW GROUP")
                prev = labels[i]
            print("\t\t {}".format( data_t[i]))

        print("X")




        print("Homogeneity: %0.3f" % metrics.homogeneity_score(labels_true, labels))
        print("Completeness: %0.3f" % metrics.completeness_score(labels_true, labels))
        print("V-measure: %0.3f" % metrics.v_measure_score(labels_true, labels))
        print("Adjusted Rand Index: %0.3f"
              % metrics.adjusted_rand_score(labels_true, labels))
        print("Adjusted Mutual Information: %0.3f"
              % metrics.adjusted_mutual_info_score(labels_true, labels))
        print("Silhouette Coefficient: %0.3f"
              % metrics.silhouette_score(X, labels))

        #############################################################################
        Plot result
        import matplotlib.pyplot as plt
       
        # Black removed and is used for noise instead.
        unique_labels = set(labels)
        colors = [plt.cm.Spectral(each)
                  for each in np.linspace(0, 1, len(unique_labels))]
        for k, col in zip(unique_labels, colors):
            if k == -1:
                # Black used for noise.
                col = [0, 0, 0, 1]
       
            class_member_mask = (labels == k)
       
            xy = df[class_member_mask & core_samples_mask]
            plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
                     markeredgecolor='k', markersize=14)
       
            xy = df[class_member_mask & ~core_samples_mask]
            plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
                     markeredgecolor='k', markersize=6)
       
        plt.title('Estimated number of clusters: %d' % n_clusters_)
        plt.show()
        print("d")
