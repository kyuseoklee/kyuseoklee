# - *- coding: utf- 8 - *-
import sys
sys.path.append('/home/coollooter')
import fitz
import os
import json
import re
import metadata_extractor.module.helpers as h
import metadata_extractor.module.xml_extraction as xml
import metadata_extractor.module.subtitles as subs
import metadata_extractor.module.deleter as delete
import metadata_extractor.module.grouper as group
import metadata_extractor.module.sectioner as section
import metadata_extractor.module.tester as test
import metadata_extractor.grobid_extraction as grobid
import metadata_extractor.module.splitter as splitter
import metadata_extractor.module.rescaler as rescaler
import metadata_extractor.module.checker as check
import metadata_extractor.module.converter as converter
import metadata_extractor.module.cleaner as cleaner
import metadata_extractor.module.extractor as ext
import metadata_extractor.module.extractor2 as ext2 # last resort subtitle algorithm is changed
import metadata_extractor.module.evaluator as evaluat
import collections
from copy import deepcopy
import pandas as pd
from sklearn.cluster import DBSCAN
from sklearn import metrics
import numpy as np
import matplotlib.pyplot as plt
import pickle
'''
    DBMETA algorithm is semi-supervised clustering algorithm for payload extraction
    Basic procedure follows as below
    1. Analysis ground truth (span information) -> hyper-parameter decision
    2. Using span unit, DBSCAN algorithm works for grouping method
    3. Classify scientific paper with subtitle and its contents
       - delete noisy span
       - select title, author(if it has), abstract(if it has)
       - re-organize subtitle section
       - subtitle extraction
       - crop its body context
'''

# TODO: correctly output the type of text and subsections
# TODO: support html along with subscripts and superscripts and unicode characters
# TODO: move abstract outside of contents
def extract(file_number, pdf, ignore_list, subsections=False, output='.json'):
    
    doc = fitz.Document(pdf); document_xml = xml.make_pages(doc); page_list = xml.make_json(document_xml)
    line_list = make_lines(page_list); 

    page_span_list = converter.add_index(line_list) # list of whole span with page with index
    page_span_list = cleaner.clean_header_footer(page_span_list)
    #dbfile=open('page_span_list','ab'); pickle.dump(page_span_list,dbfile); dbfile.close()
    #print("reading from pickle")
    #dbfile=open('page_span_list','rb'); db=pickle.load(dbfile); dbfile.close()
    span_list = converter.page_to_span(page_span_list)
    # Splitting a span(mother span) into points(daughter span)
    span_index_dict, daughter_span_list = splitter.daughter_span_generator(span_list)
    rescaled_span= rescaling_span(daughter_span_list) # rescaled + panda dataframe
    #print('clustering processing....')
    clustered_dspan= DBSCAN_operation(rescaled_span) # Clustered daughter points span using DBSCAN
    #print('post_processing....')
    mother_span_list= span_list
    mspan=post_processing(mother_span_list, clustered_dspan, span_index_dict)  # currently only return mother_span+cluster
    #mspan_origin=deepcopy.mspan
    """
    f=open('/home/coollooter/metadata_extractor/Ground_Truth/%s.spanlist'%(file_number),"w+")
    for span in mspan:
        f.writelines(str(span)+"\n")
    f.close()
    """
    mspan= cleaner.mspan_clean_delete(mspan)
    ordered_mspan= group.group_ordering(mspan)
    #for page in ordered_mspan:
    #    for group_v in page:
    #        print(group_v)
    title,author,abstract = ext.main_ext(ordered_mspan) # extract title, author, abstract using page-group ordered list
    ordered_mspan= cleaner.figure_delete(ordered_mspan) # delete figure, table group
    subtitle=ext.subtitle_ext(ordered_mspan,abstract)
    pre_text, body = ext.body_ext(ordered_mspan, subtitle, abstract) # final subtitle-body combinations
    product=[]
    product.append(title); product.append(author); product.append(abstract); product.append(pre_text); product.append(body)
    
    return product


def make_lines(document_json):
    document_list = []
    for i, page in enumerate(document_json):
        page_list = []
        for blocks in page["blocks"]:#blocks means page here
            for line in blocks["lines"]:
                for span in line["spans"]:
                    span['page'] = i + 1
                    page_list.append(span)
        document_list.append(page_list)
    return document_list


def file_list(root, recursive=True, file_types=['.pdf']):
    """
    Given a path to a directory, return a list of absolute paths to all
    of the files found within.

    :param root: An absolute or relative path to a directory
    :type root: str
    :param recursive: Whether to search the given directory recursively
    :type recursive: bool
    :return: A list of absolute paths to all files contained within
        the given directory and all of its subdirectories
    :rtype: list(str)
    """
    if os.path.isfile(root):
        return [os.path.abspath(root)]

    fpaths = []

    for abs_path, dirs, fnames in os.walk(root):
        abs_path = os.path.abspath(abs_path)
        fpaths.extend(map(lambda x: os.path.join(abs_path, x),
                          filter(lambda name: os.path.splitext(name)[1] in file_types, fnames)))

        if not recursive:
            break

    return fpaths


def send_to_json(references, json_path):
    with open(json_path, 'r') as json_file:
        data = json.load(json_file)
        data['raw_text']['extracted_sections']['references'] = references

    with open(json_path, 'r') as json_file:
        data = json.load(json_file)
        data['multiline_word_fix_text']['extracted_sections']['references'] = references
    with open(json_path, 'w') as json_file:
        json_file.write(json.dumps(data))


# TODO: call extract() rather than clean()
# TODO: ask team what is the best way to keep new subsections on mongo and show them on fingolfin

def run_DBSCAN():
    # This function follow below procedure 
    # -step1. extract metadata from pdf file 
    # -step2. gather all span list, build points list and its index lookup table
    # -step3. plug points list into DBSCAN algorithm for grouping
    products = []
    no_anal_list=[2,4,5,9,15,16,18,22,27,29,32,36,37,38,39,42,43,44,45,46,51,54,55,60,61,62,63,69,73,74,77,78,80,81,86,88,89,90,
                  96,97,99,105,109,115,117,118,119,121,125,127,128,129,130,131,132,135,138,139,140,141,142,144,145,150,153,154,155,156,157,162,163,164,165,171,170,172,173,
                  176,177,178,179,180,182]
    file_number_list=range(1,184)
    title_score=0; author_score=0; abstract_score=0; body_score=0
    file_count=0
    for i in file_number_list:
        if os.path.exists("/home/coollooter/Carlos_document/PDF/RELEVANT/{}.pdf".format(i)) == False:
            continue
        if i in no_anal_list:
            continue
        print ("file number is:", i)
        product = extract(i, "/home/coollooter/Carlos_document/PDF/RELEVANT/{}.pdf".format(i),
                            ["copyright", "journal", "publisher", "downloaded", "licensed", "download",
                            ######"doi", "ip", '■', 'XXX', 'org/', 'org'], subsections=True, output='.html')
                            "doi", "ip", '■', 'XXX', 'org/', 'org'], subsections=True, output='.json') # LKD modi line 500
        ground_t_file ="/home/coollooter/DBMETA/metadata_extractor/Ground_Truth/{}.GT_index".format(i) 
        file_count = file_count + 1
        title_result, author_result, abstract_result, body_result= evaluat.eval(product,ground_t_file)
        print('title similarity', title_result)
        print('author similarity', author_result)
        print('abstract_similarity', abstract_result)
        print('body similarity', body_result)


    return None


def rescaling_span(daughter_span_list):
    # This function put different weight on each features to help later algorithm learn correctly.
    # daughter_span_list has below information and characteristic 
    """
    x_val: x coordinate, y_val: y coordinate ==> These value ha major effect on clustering
    font: font type ==> This value has less effect  than coordinate (might be equation, or special character in the middle in the line)
    size: font size ==> Smaller size has less penalty but bigge size has more penalty so it woudld be grouped in different cluster
    index: index ==> This is used for post processing
    page: It has enormous panelty on measuring distance each page is in different cluster
    """
    pd_span_list = rescaler.convert(daughter_span_list) # convert span list to panda dataframe  
    pd_span_list = rescaler.page_scale(pd_span_list,200) # multiply 200 for page column 
    pd_span_list = rescaler.size_scale(pd_span_list,0.05,0.05) # rescale font size with below step, above step
    pd_span_list = rescaler.x_coordi_scale(pd_span_list,1) # rescale x_coordinate with scale ratio ex) ratio=0.5

    pd_span_list = pd.get_dummies(pd_span_list,prefix=['font'],drop_first=True) # hot encoding for font type
    pd_span_list = rescaler.type_scale(pd_span_list,0.1) # rescale hot-encoded dummy value 
    #TODO: later, we might need scaling font type difference.
    #print(pd_span_list)
    return pd_span_list


def DBSCAN_operation(dataframe):
    #TODO: plug all column except 'index' column  
    index_df =dataframe['index'] # extract index dataframe for later merge
    dataframe= dataframe.drop(columns=['index'])
    db = DBSCAN(eps=0.5,min_samples=1).fit(dataframe) #  User-defined EPS Value
    core_sample_mask =np.zeros_like(db.labels_,dtype=bool)
    core_sample_mask[db.core_sample_indices_]=True
    labels =db.labels_
    #print("# of daughter span points:",len(labels))
    n_clusters_ = len(set(labels)) -( 1 if -1 in labels else 0 )
    n_noise_ = list(labels).count(-1)
    #print ("the number of cluster :", n_clusters_)
    #print ("the number of noise points :", n_noise_)
    # Merge original dataframe with cluster number 
    cluster_df = pd.DataFrame({'cluster':labels})
    merged_df = pd.concat([index_df, dataframe, cluster_df],axis=1)  # ==> original data frame with index number and cluster number
    #print(merged_df) #
    return merged_df 

    # plot result
def post_processing(mother_span, clustered_dspan, index_table): 
    #print(mother_span)
    #print(index_table)
    #print(clustered_dspan)
    
    # check if one unit mother span has different cluster by daughter span when DBSCAN works.
    check.if_multispan(mother_span,clustered_dspan,index_table) # if this result show caution!, which means eps can be very small
    for i in range(len(mother_span)): # mother_span index number = i+1 
        dspan_list =index_table[i+1]; # index table searched by real index number  
        mspan_cluster_num = clustered_dspan.iloc[dspan_list[0]-1]['cluster'] # cluster of 'a' mother span ; find by first dspan cluster number assumming all the dspan cluster number would be the same in the same mother span group ( This check is done in 'if_multispan' method)
        mother_span[i]['cluster'] = int(mspan_cluster_num) # applying cluster number to the mother span
    #print(mother_span) 
    # plotting using x,y coordinate to show clustered 
    # only for 1st page
    #df = clustered_dspan[clustered_dspan['page'] ==200] # fist page extraction
    #df.to_csv('./clustered_dspan.csv') # save file 
    #df.plot(kind='scatter', x='x_val',y='y_val', c='cluster',figsize=(50,100),colormap='viridis')
    #plt.savefig('test.pdf')
    return mother_span

def sort_for_grouping(mother_span):
    page_list= [x['page'] for x in mother_span]; page_list=remove_duplicate(page_list);
    page_num= len(page_list); sorted_p = [[]] * page_num; #print(sorted_p)
    for span in mother_span:
        span_page_num=span['page'] 
        if len(sorted_p[span_page_num-1]) ==0:
            temp=[]; sorted_p[span_page_num-1]=temp; sorted_p[span_page_num-1].append(span)
        else:
            sorted_p[span_page_num-1].append(span)
    #print(sorted_p)
    final_list=[]
    for page in sorted_p:
        cluster_list=[x['cluster'] for x in page]; cluster_list=remove_duplicate(cluster_list);
        cluster_count=len(cluster_list); sorted_c=[[]]*cluster_count;
        for span in page:
            span_cluster_num=span['cluster']; index=cluster_list.index(span_cluster_num)
            if len(sorted_c[index])==0:
                temp=[]; sorted_c[index]=temp; sorted_c[index].append(span)
            else:
                sorted_c[index].append(span)
        final_list.append(sorted_c)
    return final_list

def remove_duplicate(alist):
    res=[]
    for i in alist:
        if i not in res:
            res.append(i)
    return res
        
run_DBSCAN() # point span list

