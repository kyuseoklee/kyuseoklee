
def make_lines(document_json):
    """
    Function that takes the output of PyMuPDF and extracts all spans in order to discard the order
    in which the library extracted it as. Returns a list of pages, each page is a list of spans

    :param document_json: output from PyMuPDF, dictionary
    :type document_json: dict
    :return: list(list(dict))
    """
    document_list = []
    for i, page in enumerate(document_json):
        page_list = []
        for blocks in page["blocks"]:#blocks means page here
            for line in blocks["lines"]:
                for span in line["spans"]:
                    span['page'] = i + 1
                    page_list.append(span)
        document_list.append(page_list)
    '''
    document_list = []
    for page in document_json:
        page_list = []
        for blocks in page["blocks"]:
            for line in blocks["lines"]:
                page_list.append(line)
        document_list.append(page_list)
        '''
    return document_list
