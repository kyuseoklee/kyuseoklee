import pandas as pd
import numpy as np 

def convert(daughter_span_list):
    """
    This function convert original span list to panda dataframe 
    daughter_span_list format
    [{'x_val':91.12,'index':1, 'y_val':89.12,'font':Helvetica,'page':4,'size':6.0},{'x_val'.......}....]
    """
    converted_list = pd.DataFrame(daughter_span_list)
    #print(converted_list)
    return converted_list


def page_scale(daughter_span_list,step):
    """
    page scale step : x 200, input should be panda dataframe
    """
    daughter_span_list['page']*=step
    #print(daughter_span_list)
    return(daughter_span_list)
    

def size_scale(daughter_span_list,below_step,above_step):
    """ size below the most majority font size has less penalty
    but size above the most majority font size has more penalty
    # above step = 10 # For increase size 1 from 'mode', distance gap is 10 
    """ 
    # below_step = 1 # For decrease size 1 from 'mode', distance gap is 1 
    # above step = 10 # For increase size 1 from 'mode', distance gap is 10 
    mode_size_val = daughter_span_list["size"].mode()[0]# assume font size of body 
    #print(mode_size_val)
    for index, row in daughter_span_list.iterrows():
        #print(index,row['size']*5)
        if row['size'] < mode_size_val:
            #daughter_span_list.set_value(index,'size',mode_size_val-((mode_size_val-row['size'])*below_step))  
            daughter_span_list.at[index,'size']=mode_size_val-((mode_size_val-row['size'])*below_step)  
        if row['size'] > mode_size_val:
            daughter_span_list.at[index,'size']=mode_size_val+((row['size']-mode_size_val)*above_step)
    #print(daughter_span_list)
    return daughter_span_list
    

def x_coordi_scale(daughter_span_list,ratio):
    """ x coordinte has less penalty than y coordinate with a certain ratio" 
    """
    for index,row in daughter_span_list.iterrows():
        daughter_span_list.at[index,'x_val']*=ratio
    return daughter_span_list
    
def type_scale(daughter_span_list,ratio):
    """ font type was encoded as '1' but it can be rescaled by ratio')
    """
    daughter_span_list.iloc[:,5:]*=0.1
    #print(daughter_span_list)
    return daughter_span_list

