import re
import nltk
import os
import json
import copy
def eval(product,ground_t_file,file_number,span_list):
    """ 
    here meta_evaluator.eval method is for evaluation using text format only
    """
    span_file ="/home/coollooter/DBMETA/metadata_extractor/Ground_Truth/{}.spanlist".format(file_number)
    #span_list= span_list_generator(span_file)
    DB_title=product[0]; DB_author=product[1]; DB_abstract=product[2]; DB_pre_text=product[3]; DB_sub_body=product[4]
    DB_title=DB_extract(DB_title);DB_author=DB_extract(DB_author);DB_abstract=DB_extract(DB_abstract);
    DB_pre_text= DB_para_extract(DB_pre_text); DB_subtitle, DB_body= DB_body_extract(DB_sub_body)

    GT_title, GT_author, GT_author_info, GT_abstract, GT_pre_text, GT_subtitle, GT_body= GT_extract(ground_t_file)

    #if os.path.exists("/home/coollooter/Carlos_document/JSON/RELEVANT/MATESC/{}.json".format(file_number)) == False:
    if os.path.exists("/home/coollooter/DBMETA/metadata_extractor/MATESC_output/{}.json".format(file_number)) == False:
        print('MATESC file does not exists')
    #MT_file= "/home/coollooter/Carlos_document/JSON/RELEVANT/MATESC/{}.json".format(file_number)
    MT_file= "/home/coollooter/metadata_extractor/Final_output/METASC/{}.json".format(file_number)
    
    MT_title, MT_author, MT_abstract, MT_dict= METASC_file_converter(MT_file)
    
    # below is comparison result for METASC vs GT 
    print('MT evaluation vs GT')
    MT_title_score=MT_text_main_measure(MT_title,GT_title,span_list)
    print('title:',MT_title_score)
    MT_author_score=MT_text_main_measure(MT_author,GT_author,span_list)
    print('author:',MT_author_score)
    MT_abstract_score=MT_text_main_measure(MT_abstract,GT_abstract,span_list)
    print('abstract:',MT_abstract_score)
    MT_body_score= MT_text_body_measure(MT_dict, GT_pre_text, GT_subtitle, GT_body, span_list)
    print('body:',MT_body_score)
    print('METASC_total_Score', MT_title_score+MT_author_score+MT_abstract_score+MT_body_score)

    print('DBMETA evaluation vs GT')
    DB_title_score=DB_text_main_measure(DB_title,GT_title,span_list) 
    print('title:',DB_title_score)
    DB_author_score=DB_text_main_measure(DB_author,GT_author,span_list) 
    print('author:',DB_author_score)
    DB_abstract_score=DB_text_main_measure(DB_abstract,GT_abstract,span_list) 
    print('abstract:',DB_abstract_score)
    DB_body_score=DB_text_body_measure(DB_pre_text,GT_pre_text,DB_subtitle,GT_subtitle,DB_body,GT_body,span_list)
    print('body:', DB_body_score)
    print('DBMETA_total_Score', DB_title_score+DB_author_score+DB_abstract_score+DB_body_score)
    return None

def DB_text_body_measure(DB_pre_text,GT_pre_text,DB_subtitle,GT_subtitle,DB_body,GT_body,span_list):
    if len(GT_pre_text)==0:
        subtitle_number = len(GT_subtitle)
    else:
        subtitle_number = len(GT_subtitle)+1
    total_similarity=0
    for i,subtitle_GT in enumerate(GT_subtitle):
        GT_subtitle_text= group_text_extract(subtitle_GT,span_list)
        for j,subtitle_DB in enumerate(DB_subtitle):
            DB_subtitle_text = group_text_extract(subtitle_DB,span_list)
            #print('GT_subtitle_text',GT_subtitle_text)
            #print('DB_subtitle_text',DB_subtitle_text)
            if text_compare(GT_subtitle_text,DB_subtitle_text)[0] == True:
                #print('GT_subtitle in DBMETA',GT_subtitle_text)
                GT_body_text=group_text_extract(GT_body[i],span_list)
                #print('GT_body_text',GT_body_text)
                DB_body_text=group_text_extract(DB_body[j],span_list)
                #print('DB_body_text',DB_body_text, DB_body[j])
                score_body=text_compare(GT_body_text,DB_body_text)[1]
                similarity_body=(1-score_body)*100
                #print('body_similarity',similarity_body)
                break
            else:
                similarity_body=float(0)
                continue
        total_similarity=total_similarity+similarity_body
    total_body_similarity=total_similarity/subtitle_number
    #print('Total_body_similarity',total_body_similarity)
    return total_body_similarity




def MT_text_body_measure(compare_obj,GT_pre_text, GT_subtitle, GT_body, span_list):
    MT_body =copy.deepcopy(compare_obj)
    del MT_body['title']; del MT_body['authors']; 
    if 'abstract' in MT_body.keys(): 
        del MT_body['abstract']
    MT_subtitle_list = MT_body.keys()
    #print('MT_subtitle_list', MT_subtitle_list)
    #print(GT_pre_text)
    if len(GT_pre_text) == 0:
        subtitle_number = len(GT_subtitle)
    else:
        subtitle_number = len(GT_subtitle) +1 
    total_similarity=0
    for i, subtitle_GT in enumerate(GT_subtitle) :
        GT_subtitle_text= group_text_extract(subtitle_GT,span_list)
        for MT_subtitle in MT_subtitle_list:
            if text_compare(GT_subtitle_text,MT_subtitle)[0] == True:
                #print('GT_subtitle_MT',GT_subtitle_text)
                #print('GT_its_body_text', GT_body[i])
                MT_its_body_text=MT_body[MT_subtitle]; GT_its_body_text=group_text_extract(GT_body[i],span_list)
                #print('MT_its_body', MT_its_body_text)
                #print('GT_its_body', GT_its_body_text)
                score_body= text_compare(GT_its_body_text,MT_its_body_text)[1]
                similarity_body=(1-score_body)*100
                #print('body_similarity',similarity_body)
                break
            else:
                similarity_body=float(0)
                continue
        total_similarity=total_similarity+similarity_body
    total_body_similarity=total_similarity/subtitle_number
    #print('Total_Body similarity', total_body_similarity)
    return total_body_similarity


def text_compare(GT_subtitle_text,MT_subtitle):
    #print('GT_subtitle_text',GT_subtitle_text)
    GT_temp = re.sub(r' ',"",GT_subtitle_text); GT_text=GT_temp.lower()
    #print('GT_subtitle',GT_text)
    if MT_subtitle==None:
        MT_text=""
    else:
        MT_temp = re.sub(r' ',"",MT_subtitle); MT_text=MT_temp.lower()
    #print('MT_subtitle',MT_text) # already text ex)u'author information
    length1=len(GT_text); length2=len(MT_text);
    if length1 > length2:
        GT_length= length1; 
    else:
        GT_length= length2
    result=nltk.edit_distance(GT_text,MT_text); #print(result)
    if float(result)/float(GT_length) < 0.2 :
        #print(GT_text, MT_text, 'MATCHED')
        return True, float(result)/float(GT_length) 
    else: 
        return False, float(result)/float(GT_length)

def group_text_extract(subtitle_GT,spanlist):
    #print('subtitle_GT in group_text_extract function', subtitle_GT)
    if len(subtitle_GT) ==1:
        #temp_text=next(item for item in spanlist if item['index']==int(subtitle_GT[0]))
        temp_text=index_text_converter(subtitle_GT[0],spanlist)
        text= modi_text(temp_text['text'])
        #print(text)
        return text
    elif len(subtitle_GT) > 1:
        text=""
        for i,element in enumerate(subtitle_GT):
            if i ==0 :
                #temp_text= next(item for item in spanlist if item['index']==int(element))['text']
                temp_text= index_text_converter(element,spanlist)
                text = modi_text(temp_text['text'])
            else:
                #print(element)
                #temp_text = next(item for item in spanlist if item['index']==int(element))['text']
                temp_text = index_text_converter(element,spanlist)
                #print('temp_text',temp_text)
                text = text + modi_text(temp_text['text'])
        #print(text)
        return text

def index_text_converter(index,spanlist):
    #for span in spanlist:
    #    print('span:', span)
    dummy_span={}; dummy_span['text']=""
    for i,span in enumerate(spanlist):
        if int(span['index']) == int(index):
                return span
        else: 
            continue
    #print('no index found: check needed','index number:',index)
    return dummy_span


def MT_text_main_measure(compare_obj,GT_obj,span_list):
    GT_list=GT_obj
    if len(GT_list) ==0 : 
        print("Ground_Truth is empty: check this")
    GT_text=""
    for span in span_list:
        index= int(span['index'])
        if index in GT_list: 
            #print('index:', GT_list.index(index),index)
            if GT_list.index(index) == 0:
                GT_text=repr(span['text'])[2:-1]
                continue
            else:
                GT_text=GT_text+" "+repr(span['text'])[2:-1]
                continue
    GT_text=modi_text(GT_text) # string
    if type(compare_obj) == unicode:
        Com_text=repr(compare_obj)[2:-1]
    else:
        Com_text=str(compare_obj) # string
    result= similarity_main_measure(GT_text,compare_obj) 
    return result

def DB_text_main_measure(DB_obj,GT_obj,span_list): # here both object are list-based information
    #print('DB_obj', DB_obj)
    #print('GT_obj', GT_obj)
    GT_list=GT_obj; DB_list=DB_obj; GT_text=""
    if DB_obj == GT_obj :
        return 100
    for span in span_list:
        index=int(span['index'])
        if index in GT_list:
            if GT_list.index(index)==0:
                GT_text =repr(span['text'])[2:-1]
                continue
            else:
                GT_text=GT_text+" "+repr(span['text'])[2:-1]
                continue
    GT_text=modi_text(GT_text)
    #print('GT_text', GT_text)
    DB_text=""
    for span in span_list:
        index=int(span['index'])
        if index in DB_list:
            if DB_list.index(index)==0:
                DB_text=repr(span['text'])[2:-1]
                continue
            else:
                DB_text=DB_text+" "+repr(span['text'])[2:-1]
                continue
    DB_text=modi_text(DB_text)
    result=similarity_main_measure(GT_text,DB_text)
    return result

def modi_text(text):
    #print('Pre_text', text)
    temp1=re.sub(r'[\\]u[a-zA-Z0-9]{4}',"",text); temp2=re.sub(r'[\\]x[a-zA-Z0-9]{2}',"",temp1)
    temp3=str(repr(temp2)[2:-1])
    temp4=re.sub(r'[\\]u[a-zA-Z0-9]{4}',"",temp3)
    #print('post_text', temp4)
    return temp4
    




def METASC_file_converter(METASC_file):
    with open(METASC_file) as f:
        lines=f.read().splitlines()
        MT_source=lines[0]
        obj=json.loads(MT_source)
    MT_title = obj['title']; MT_author = obj['authors']; 
    if 'abstract' in obj.keys():
        MT_abstract= obj['abstract']
    else:
        MT_abstract = []

    return MT_title, MT_author, MT_abstract, obj


    #print(obj['title']); print(obj['authors']); print(obj['abstract'])

def similarity_body_measure(DB_pre_text,GT_pre_text,DB_subtitle,GT_subtitle,DB_body,GT_body):
    total_score=0
    gt_subtitle_num=len(GT_subtitle); # whole body number 
    if len(GT_pre_text) !=0:
        total_num=gt_subtitle_num+1
        pre_text_result=similarity_main_measure(DB_pre_text,GT_pre_text)
        total_score=total_score + (pre_text_result/total_num)
    else:
        total_num=gt_subtitle_num
        pre_text_result=0
    for i,subtitle in enumerate(GT_subtitle): 
        gt_sub = subtitle ; 
        if gt_sub in DB_subtitle:
            db_sub_index = DB_subtitle.index(gt_sub)
            result=similarity_main_measure(GT_body[i],DB_body[db_sub_index])
            #print('body_part:', GT_body[i], DB_body[db_sub_index], result)
            total_score= total_score + (result/total_num)
        else: 
            a=1
    return total_score
    

def similarity_main_measure(alist, blist):
    A=len(alist); B=len(blist)
    if B > A :
        temp=alist; alist=blist; blist=temp
    A=len(alist)
    result=nltk.edit_distance(alist, blist)
    #print('inner result', result)
    return 100-(float(result)/float(A)*100)

def DB_body_extract(DB_sub_body):
    subtitle_list=[]; body_list=[]
    for i,sub_combi in enumerate(DB_sub_body):
        subtitle=sub_combi[0]; paragraph= sub_combi[1]
        temp_sub=DB_extract(subtitle); 
        temp_para=DB_para_extract(paragraph);
        subtitle_list.append(temp_sub); body_list.append(temp_para)
    return subtitle_list, body_list
    
def DB_para_extract(paragraph):
    res=[]
    for group in paragraph:
        for span in group:
            index= span['index']; res.append(index)
    return res
        
def DB_extract(DB_text): # for title, author, abstract, pre_text # for one group 
    #print(DB_text)
    res=[]
    if DB_text==None:
        res=[]
    else:
        for span in DB_text:
            index = span['index'] ; res.append(index)
    return res


def GT_extract(ground_t_file):
    author_info_status=False;pre_text_status=False
    with open(ground_t_file) as f :
        lines=f.read().splitlines()
        for i in range(len(lines)):
            if i == 0 :
                continue
            tok=lines[i].split("[")
            if len(tok[0])!=0: # when it has title ex) abstract = [ xxx , xx]
                a=1
                if tok[0].find('title')==0  : 
                    temp_title_index=tok[1]; title_index=temp_title_index.split("]")[0]
                    title=paragraph_index_rephrase(title_index)
                if tok[0].find('author=') > -1 :
                    temp_author_index=tok[1]; author_index=temp_author_index.split("]")[0]
                    #print('author_index',author_index)
                    author=paragraph_index_rephrase(author_index)
                if tok[0].find('author_info') > -1 :
                    temp_author_info_index=tok[1]; author_info_index=temp_author_info_index.split("]")[0]
                    author_info=paragraph_index_rephrase(author_info_index)
                    author_info_status=True
                if tok[0].find('abstract') > -1:
                    temp_abstract_info_index=tok[1]; abstract_info_index=temp_abstract_info_index.split("]")[0]
                    abstract=paragraph_index_rephrase(abstract_info_index)
                if tok[0].find('subtitle_list')> -1:
                    temp_subtitle_list_index=tok[1]; subtitle_list_index= temp_subtitle_list_index.split("]")[0]
                    subtitle_line_index=i; subtitle_list=subtitle_index_rephrase(subtitle_list_index)
                if tok[0].find('pre_text') > -1:
                    temp_pre_text_index=tok[1]; pre_text_index=temp_pre_text_index.split("]")[0]
                    pre_text= paragraph_index_rephrase(pre_text_index)
                    pre_text_status=True
        subtitle=subtitle_list_index.split(",")
        num_subtitle=len(subtitle)
        body=[]
        for i in range(num_subtitle):
           temp= lines[i+1+subtitle_line_index]; temp=temp[1:-1]
           para = paragraph_index_rephrase(temp)  
           body.append(para)
           
        #print('title',title)
        #print('author', author)
        #print('author_info', author_info)
        #print('abstract', abstract)
        if author_info_status==True:
            a=1
        else:
            author_info=[]
        if pre_text_status ==True :
            a=1
        else:
            pre_text=[]
        #print('subtitle_list', subtitle_list)
        #for i in range(len(body)):
        #    print('body',body[i])
    return title, author, author_info, abstract, pre_text, subtitle_list, body 

def subtitle_index_rephrase(subtitle_index): 
    res=[]; tok=subtitle_index.split(","); num_tok=len(tok)
    for i in range(num_tok):
        index_list=[]
        if tok[i].find(':') > -1:
            index=tok[i].split(':'); left_index=index[0]; right_index=index[1]
            for j in range(int(left_index),(int(right_index)+1)):
                index_list.append(int(j))
        else:
            index_list=[int(tok[i])]
        res.append(index_list)
    return res



def paragraph_index_rephrase(pseudo_index): # pseudo_index 69:117,130:140
    tok=pseudo_index.split(","); num_tok= len(tok)
    res=[];index_list=[]
    for i in range(num_tok):
        #print(tok[i])
        index_list=[]
        if tok[i].find(':') > -1:
            index=tok[i].split(':'); left_index=index[0]; right_index=index[1]
            for j in range(int(left_index),(int(right_index)+1)):
                index_list.append(j)
        else:
            index_list=[int(tok[i])]
        res=res+index_list
    return res


def span_list_generator(span_file):
    span_list=[]
    with open(span_file) as f:
        lines=f.read().splitlines()
        for i in range(len(lines)):
            temp=json.loads(lines[i])
            span_list.append(temp)
    return span_list
                
              
                
