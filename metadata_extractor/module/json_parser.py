
# read_json function
"""
only accept one line json format 
"""

def read_json(json_file): # read ground_truth, only working for 1 line json file
    key_list=[]
    text_dict={}
    with open(json_file) as f :
        lines=f.read().splitlines()
        for i in range(len(lines)):
            line=lines[i]
            if i > 1 : 
                print("File has more than two lines", json_file)
            indices = [ i for i, x  in enumerate(line) if x =='"']
            #print(indices)
        temp=[]
        for j in range(len(indices)/2):
            start_index = j*2; start=indices[start_index]+1;
            end_index =j*2+1; end=indices[end_index]-1;
            #print(line[start:end+1]) 
            temp.append(line[start:end+1])
        for k in range(len(temp)):
            if k%2 == 0:
                key=temp[k]; key_list.append(key);
                text=temp[k+1]; text_dict[key]=text
        #print(key_list)
        #print(text_dict)
    return key_list, text_dict


def format_converter(file): # convert format as same as gt_file
    key_list=[]
    ext_dict={}
    with open(file) as f:
        lines=f.read().splitlines()
        for i in range(len(lines)):
            line=lines[i]
            if i>1:
                print("File has more than two lines", file)
            indices =[i for i, x in enumerate(line) if x=='"']
            print (indices)
            braket_indices=[i for i, x in enumerate(line) if x=='{' or x=='}']
            print (braket_indices)
