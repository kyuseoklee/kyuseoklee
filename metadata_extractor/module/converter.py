

def add_index(page_span_list):
    new_list=[]
    index=1
    for i,page in enumerate(page_span_list):
        page_temp=[]
        new_list.append(page_temp)
        for span in page:
            span['index']=index; index=index+1
            new_list[i].append(span)
    return new_list




def page_to_span(page_span_list):
    # it convert block+ span list into spans only list 
    new_list=[]
    for page in page_span_list:
        for span in page:
            new_list.append(span)
    for i in range(len(new_list)):
        new_list[i]['index']=i+1
    return new_list

def span_to_page(span_list):
    page_list=[]
    for span in span_list:
        if span['page'] not in page_list:
            page_list.append(span['page'])
    page_num=len(page_list)
    new_page_span_list=[]
    for i in page_num:
        temp=[]
        for span in span_list:
            if span['page']==i+1:
                temp.append(span)
        new_page_span_list.append(temp)
    return new_page_span_list



def make_lines(document_json):
    """
    Function that takes the output of PyMuPDF and extracts all spans in order to discard the order
    in which the library extracted it as. Returns a list of pages, each page is a list of spans

    :param document_json: output from PyMuPDF, dictionary
    :type document_json: dict
    :return: list(list(dict))
    """
    document_list = []
    for i, page in enumerate(document_json):
        page_list = []
        for blocks in page["blocks"]:#blocks means page here
            for line in blocks["lines"]:
                for span in line["spans"]:
                    span['page'] = i + 1
                    page_list.append(span)
        document_list.append(page_list)
    '''
    document_list = []
    for page in document_json:
        page_list = []
        for blocks in page["blocks"]:
            for line in blocks["lines"]:
                page_list.append(line)
        document_list.append(page_list)
        '''
    return document_list


