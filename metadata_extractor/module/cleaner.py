import metadata_extractor.module.converter as converter
import math
import re


def figure_delete(ordered_mspan): # page-group ordered list
    fig_word=['fig','figure','pic','table']
    fig_group=[]
    for page in ordered_mspan:
        for group in page:
            if measure_area(group) > 400:
                continue
            if len(group) > 1:
                text = repr(group[0]['text'].lower())[2:-1] # finding alphabetical text, not special character 
                temp = re.sub(r'[\\]u[a-zA-Z0-9]{4}',"",text)
                temp2 =re.sub(r'[\\]x[a-zA-A0-9]{2}',"",temp)
                for ignore_word in fig_word:
                    index= temp2.find(ignore_word)
                    if index > -1 and index < 3  :
                        #print('index', index, 'temp2', temp2, 'ignore_word:',ignore_word)
                        fig_group.append(group)
                        #print('fig group', group)
                        break
    
    group_list=page_to_group_only(ordered_mspan)
    fig_index_list=[]
    for fig in fig_group:
       fig_index_list.append(group_list.index(fig))
    fig_index_list=sorted(fig_index_list, key=lambda i :i)
    for index in sorted(fig_index_list, reverse=True):
        del group_list[index]
    re_ordered_mspan = group_only_to_page(group_list)
    return re_ordered_mspan

def measure_area(group):
    area=0
    for span in group:
        span_x = span['bbox'][2]-span['bbox'][0]
        span_y = span['bbox'][2]-span['bbox'][1]
        span_area= span_x * span_y ; area= area + span_area
    return area


def group_only_to_page(group_list):
    page_list=[]
    for group in group_list:
        if group[0]['page'] not in page_list:
            page_list.append(group[0]['page'])
    page_num=len(page_list)
    new_page_group_list=[]
    for i in range(page_num):
        temp=[]
        for group in group_list:
            if group[0]['page']==i+1:
                temp.append(group)
        new_page_group_list.append(temp)
    return new_page_group_list



def page_to_group_only(page_group_list):
    new_list=[]
    for page in page_group_list:
        for group in page:
            new_list.append(group)
    return new_list


def count_alpha(string):
    count=0
    for a in string:
        if (a.isalpha())==True:
            count+=1
    return count

def finding_first_text(group):
    text_list=[]
    for span in group:
        #print(span)
        text = span['text'];r_text=repr(text)[2:-1]; 
        temp =re.sub(r'[\\]u[a-zA-Z0-9]{4}',"",r_text)
        temp2= re.sub(r'[\\]x[a-zA-Z0-9]{2}',"",temp)
        if count_alpha(temp2) >2 :
            first_text=span['text']
            text_list.append(first_text)
        else:
            continue
    if len(text_list) == 0 :
        text_list.append('aaaaaa')

    return text_list[0]
        
    



def mspan_clean_delete(mspan):
    span_list= mspan
    # span level cleaning process after clustering
    #  "repetative span"
    # "possible header and footer"
    #for span in span_list:
    #    print(span)

    clean_list1=[]; clean_list2=[]; clean_list3=[]; clean_list4=[]; clean_list5=[];
    for spanA in span_list:
        for spanB in span_list:
            if spanA['index'] == spanB['index']:
                continue
            check1 = spanA['text']==spanB['text'];check2 = spanA['bbox']==spanB['bbox']
            check3 = spanA['font']==spanB['font'];check4 = spanA['size']==spanB['size']
            check6 = spanA['bbox'][0]; check7 = spanA['bbox'][1]
            check8 = spanA['bbox'][2]; check9 = spanA['bbox'][3]
            # same position in every page : mark
            if check1 & check2 & check3 & check4: 
                clean_list1.append(spanA); clean_list1.append(spanB)
        # header or footer
        if float(check6) < 5 :
            clean_list2.append(spanA)
            continue
        if float(check8) > 95  :  
            clean_list3.append(spanA)
            continue
        if float(check7) < 5 :
            clean_list4.append(spanA)
            continue
        if float(check9) > 95 :
            clean_list5.append(spanA)
            continue
    clean_list = clean_list1 + clean_list2 + clean_list3 + clean_list4 + clean_list5
    clean_list=remove_duplicate(clean_list)
    clean_list =sorted(clean_list, key=lambda i:int(i['index']))
    clean_index_list=[]
    for span in clean_list:
        clean_index_list.append(span_list.index(span))
    for index in sorted(clean_index_list, reverse=True):
        #print("removed", span_list[index])
        del span_list[index]
    return span_list 




def clean_ignore(page_mspan):
    """ clean 1. keyword
              2. email address, url only  telephone 
              3. ignore ( reserved, received, accepted. All rights, tell, fax, tellephone

    """
    ignore_list =['reserved','received','accepted','published','all rights','tell','fax','tellephone','keywords','e-mail','doi','www.','.org']
    remove_list=[]
    for i,page in enumerate(page_mspan):
        for group in page :
            group_size= size_checker(group)
            if group_size > 400 : # This number is approximately minimun abstract group size 
                continue
            for span in group:
                text = span['text'].lower(); 
                for ignore_word in ignore_list:
                    if text.find(ignore_word) >= 0 :
                        temp=[]; temp.append(i); temp.append(group)
                        remove_list.append(temp)
    remove_list=remove_duplicate(remove_list)
    for element in remove_list:
       page_num=element[0]; group=element[1]
       page_mspan[page_num].remove(group)
    #for page in page_mspan:
    #    for group in page:
    #        print(group)
        
    return page_mspan
                    

def filter_span(span):
    text= span['text']
    #print(text)
                            
def size_checker(group):
    square=0
    for span in group:
        square=square + ( (span['bbox'][3]-span['bbox'][1])*(span['bbox'][2]-span['bbox'][0]))
    return square

def clean_header_footer(page_span_list):
    # Cleans header and footer using below criteria
    # 1. repetive word,font size,font type,location but only different page number -> header and footer
    # 2. location is > 90
    span_list=converter.page_to_span(page_span_list)
    #print("clean header and footer processing")
    clean_list1=[] # this is for step1 :exactly repetive span per page
    clean_list2=[] # this is for step2 :not exactly repetive location per page   
    for spanA in span_list:
        for spanB in span_list:
            if spanA['index'] == spanB['index']:
                continue
            check1 = spanA['text']==spanB['text'];check2 = spanA['bbox']==spanB['bbox']
            check3 = spanA['font']==spanB['font'];check4 = spanA['size']==spanB['size']
            check5 = spanA['page']!=spanB['page'];check6 = spanA['bbox'][1]; check7 = spanA['bbox'][3]
            if check1 & check2 & check3 & check4:
                clean_list1.append(spanA); clean_list1.append(spanB)
            if (check1 & check3 & check4 &check5 ) and (check2==False) and ( (float(check6) > 93) or (float(check6) < 7))  : 
               temp1=(spanA['bbox'][0]-spanB['bbox'][0]); temp2=(spanA['bbox'][1]-spanB['bbox'][1])
               temp=math.sqrt((temp1*temp1)+(temp2*temp2))
               if temp <10 : 
                   #print(spanA)
                   #print(spanB)
                   clean_list2.append(spanA); clean_list2.append(spanB);
    clean_list = clean_list1 + clean_list2
    clean_list=remove_duplicate(clean_list)
    clean_index_list=[]
    for element in clean_list :
        clean_index_list.append(element['index'])     
    for page in page_span_list:
        for span in page:
            if span['index'] in clean_index_list:
                page.remove(span)
    return page_span_list 
                
def remove_duplicate(alist):
    res=[]
    for i in alist:
        if i not in res:
            res.append(i)
    return res

            
