import numpy as np
import re
import math
import unicodedata
import nltk
from nltk.corpus import wordnet
import spacy

def body_ext(ordered_mspan, subtitle_list, abstract):
    group_list= page_to_group(ordered_mspan)
    #print(subtitle_list)
    if abstract==None:
        abstract_group_index=1
        #print('abstract_group_index is None')
    elif abstract not in group_list:
        abstract_group_index=1
    else:
        abstract_group_index = group_list.index(abstract)
    if len(subtitle_list) ==0 :
        pre_text=group_list[abstract_group_index+1:]
        body=[]
    else:
        #print(subtitle_list[0])
        first_subtitle_index = group_list.index(subtitle_list[0])
        pre_text=group_list[abstract_group_index+1:first_subtitle_index]
        body=[]
        for i,subtitle in enumerate(subtitle_list):
            if subtitle != subtitle_list[-1]:
                temp=[]; subtitle_index= group_list.index(subtitle); next_subtitle_index = group_list.index(subtitle_list[i+1])
                temp.append(subtitle); paragraph= group_list[subtitle_index+1:next_subtitle_index]
                temp.append(paragraph)
                body.append(temp)
            else: 
                temp=[]; subtitle_index = group_list.index(subtitle);
                temp.append(subtitle); paragraph= group_list[subtitle_index+1:]
                temp.append(paragraph)
                body.append(temp)
    #print('pre_text', pre_text)
    #print('body', body)
            
    return pre_text, body # body has list of ['subtitle','its body']
                
            

def num_alphabet(group):
    alphabet_count=0
    for span in group:
        text=repr(span['text'])[2:-1]; temp1=re.sub(r'[\\]u[a-zA-Z0-9]{4}'," ",text)
        temp2 =re.sub(r'[\\]x[a-zA-Z0-9]{2}'," ",temp1)
        for i in range(len(temp2)):
            if (temp2[i].isalpha()):
                alphabet_count = alphabet_count +1 
    return alphabet_count

def corner_check(group):
    corner_STATUS= False
    min_x_val, max_x_val, min_y_val, max_y_val = finding_min_max_in_group(group)
    center_x = (min_x_val+max_x_val)/2
    center_y = (min_y_val+max_y_val)/2
    if center_x > 94 or center_x < 6:
        corner_STATUS=True
    if center_y > 94 or center_y < 6:
        corner_STATUS=True
    return corner_STATUS 

def finding_min_max_in_group(group):
    min_x_val=0; max_x_val=100; min_y_val=0; max_y_val=100
    for i,span in enumerate(group):
        if i==0:
            min_x_val = span['bbox'][0]; max_x_val =span['bbox'][2]
            min_y_val = span['bbox'][1]; max_y_val =span['bbox'][3]
        else:
            if span['bbox'][0] < min_x_val:
                min_x_val= span['bbox'][0]
            if span['bbox'][2] > max_x_val:
                max_x_val= span['bbox'][2]
            if span['bbox'][1] < min_y_val:
                min_y_val= span['bbox'][1]
            if span['bbox'][3] > max_y_val:
                max_y_val= span['bbox'][3]
    return min_x_val, max_x_val, min_y_val, max_y_val

def subtitle_ext(ordered_mspan,abstract):
    """
    1. sequence :later than abstract
    2. area : > 100
    3. ignore_list = [fig, figure, pic, table]
    4. length, isalpha > # , bold type
    """
    group_list= page_to_group(ordered_mspan)
    #for group in group_list:
    #    print(group)
    if abstract == None:
        abstract_index=0
    elif abstract not in group_list:
        abstract_index=0
    else:
        abstract_index=group_list.index(abstract)
        #print('abstract_index:', abstract_index)
    subtitle_candidates=[]
    for i,group in enumerate(group_list) :
        #print(group)
        if i <= abstract_index : 
            #print(" i <=abstract_index")
            continue
        if measure_area(group) > 100 :
            #print(" area > 100")
            continue
        if len(group) > 4:  # seems like equation or so many special character
            #print("group length > 4")
            continue
        num_alpha = num_alphabet(group)
        if num_alpha < 3 or num_alpha > 40: # seems like special character group or equation index or page
            #print( "num_alpha < 3 or >50")
            continue             
        #if bold_type_check(group)==False:
        #    print(" no bold type")
        #    continue
        subtitle_candidates.append(group)
        if corner_check(group) == True:
            continue
    #print('pre-subtitle_candidates',subtitle_candidates)
    no_subtitle_list=[] # index in current subtitle candidates
    for group in subtitle_candidates:
        subtitle_index=group_list.index(group)
        if subtitle_index+1 == len(group_list):
            no_subtitle_list.append(subtitle_candidates.index(group))
            print('1 added :',group)
            continue
        # pre_subtitle check if subtitle is just as fragment in the middle of sentence 
        pre_subtitle = group_list[subtitle_index-1]
        #print(pre_subtitle)
        pre_min_y= pre_subtitle[-1]['bbox'][1]; pre_max_y=pre_subtitle[-1]['bbox'][3]
        center_y = (pre_min_y + pre_max_y)/2
        min_x_val, max_x_val, min_y_val, max_y_val = finding_min_max_in_group(group)
        if (center_y > min_y_val) and (center_y < max_y_val) and (measure_area(pre_subtitle) > 1.8)  :
            no_subtitle_list.append(subtitle_candidates.index(group))
            print('2 added:',group)
            continue 

        post_subtitle = group_list[subtitle_index+1]
        post_min_y= post_subtitle[0]['bbox'][1]; post_max_y=post_subtitle[0]['bbox'][3]
        center_y = (post_min_y + post_max_y)/2
        min_x_val, max_x_val, min_y_val, max_y_val = finding_min_max_in_group(group)
        if (center_y > min_y_val) and (center_y < max_y_val) :
            no_subtitle_list.append(subtitle_candidates.index(group))
            print('3 added', group)
            continue
        for span in group:
            if (span['text'].find('@') > -1) or (span['text'].lower().find('.com') > -1) or (span['text'].lower().find('.org')> -1):
                no_subtitle_list.append(subtitle_candidates.index(group))
                print('4 added', group)
            text=repr(span['text'])[2:-1]; temp= re.sub("\s","",text) 
            if ( temp.lower().find('abstract') > -1) : 
                no_subtitle_list.append(subtitle_candidates.index(group))
        current_subtitle=group
        for compare_subtitle in subtitle_candidates:
            if int(current_subtitle[0]['index']) == int(compare_subtitle[0]['index']):
                continue
            if current_subtitle[0]['text'] == compare_subtitle[0]['text'] :
                no_subtitle_list.append(subtitle_candidates.index(current_subtitle))
                no_subtitle_list.append(subtitle_candidates.index(compare_subtitle))
                continue
        # shape check(vertical sentence filtering)
        if abs(max_x_val-min_x_val)*1.5 < abs(max_y_val-min_y_val) :
            print('5 added', group)
            no_subtitle_list.append(subtitle_candidates.index(group))

    no_subtitle_list= remove_duplicate(no_subtitle_list)
    if len(no_subtitle_list) > 0:
        for no_subtitle_index in sorted(no_subtitle_list, reverse=True):
            #print(no_subtitle_index)
            del subtitle_candidates[int(no_subtitle_index)]
    
    #for subtitle in subtitle_candidates:
    #    print('subtitle:', subtitle)
    
    # extract largest size group
#    if len(subtitle_candidates) > 0:
#        max_sub_size = max([x[0]['size'] for x in subtitle_candidates])
#    else: 
#        max_sub_size = 100
#    no_sub_list=[]
#    for subtitle in subtitle_candidates:
#        if subtitle[0]['size'] < max_sub_size:
#            no_sub_list.append(subtitle_candidates.index(subtitle))
#    if len(no_sub_list) > 0:
#        for no_sub in sorted(no_sub_list, reverse=True):
#            del subtitle_candidates[int(no_sub)]
#    subtitle_candidates= subtitle_index_analyzer(subtitle_candidates)
    #for subtitle in subtitle_candidates:
    #    print('subtitle:', subtitle)

    for subtitle in subtitle_candidates:
        print('subtitle', subtitle)
    return subtitle_candidates
    
    
def subtitle_index_analyzer(subtitle_candidates):
    no_sub_index=[]
    for subtitle in subtitle_candidates:
        text= subtitle[0]['text']
        text= repr(subtitle[0]['text'])[2:-1]
        temp =re.sub(' ','',text); sub_category_index=re.findall(r'^[^.]+[\.][0-9]',temp)
        if len(sub_category_index) > 0:
            no_sub_index.append(subtitle_candidates.index(subtitle))
    if len(no_sub_index) >0:
        for no_sub in sorted(no_sub_index, reverse=True):
            del subtitle_candidates[int(no_sub)]
    return subtitle_candidates
        
            #temp1=re.sub(r'[\\]u[a-zA-Z0-9]{4}'," ",r_text);temp2=re.sub(r'[\\]x[a-zA-Z0-9]{2}'," ",temp1)
    
        
def remove_duplicate(alist):
    res=[]
    for i in alist:
        if i not in res:
            res.append(i)
    return res
        

def bold_type_check(group):
    for span in group:
        font_size=span['font']; tok=font_size.split("-"); thick=tok[-1]
        if thick[0]=='B' or thick[0]=='b': # bold type
            return True
        else:
            return False



def main_ext(ordered_mspan): # page-group ordered list
    group_list=page_to_group(ordered_mspan)
    title = title_ext(group_list) # title_group extracted "span-ordered list"
    #print("Title: ", title)
    author= author_ext(group_list, title)  
    #print("Author:", author) 
    origin_abstract= abstract_ext(group_list)
    #text_in_abs= abstract[0]['text'].lower(); first_text=re.sub(r"\s+","",text_in_abs)
    #if first_text.find('abstract') > -1: # if abstract keyword is in group
    #    abtract=abstract[1:]
    #print('abstract:', abstract)
    return title, author, origin_abstract

def abstract_ext(group_list):
    """
    # criterion
    # 'abstract keyword': distance with group is considered 
    # 'paragraph size' min area = 180, max area = 1500
    # 'order( sequence, page)
    # 'non-person's name included' ( scoring )
    # font-size (not too big)
    """
    abstract_candidates=[]; search_STATUS =0 
    # search_STATUS 0: not started,            1: 'abstract' keyword only found,
    #               2: 'abstract'with context group found ( need only seperation ) 
    #               3: abstract_candidates found ( need further investigation ) 
    for i,group in enumerate(group_list):
        if group[0]['page'] >2 :
            continue
        area =measure_area(group); temp=group[0]['text'].lower(); first_text=re.sub(r"\s+","",temp)
        if first_text.find('abstract') > -1:
            if area < 50 :  # means only title group 
                keyword_group = group
                search_STATUS = 1
                break
            if area > 180 and area < 1500: 
                final_abstract = group # but it has 'abstract:' and context)
                search_STATUS = 2 
                break 
        else: 
            if area > 180 and area < 1500: # least information of abstract
                abstract_candidates.append(group)
                search_STATUS =3 

    if search_STATUS == 2:
        final_abstract= group
        return final_abstract
    if search_STATUS == 1:
        key_word_index = group_list.index(keyword_group)
        final_abstract = group_list[key_word_index+1]
        return final_abstract

    if search_STATUS == 3: 
        final_candidates=[] 
        for i, group in enumerate(abstract_candidates):
            temp=[]
            name_local_frequency= measure_name_local_info(group)
            temp.append(group); score= name_local_frequency + i*5 + group[0]['size']*5
            temp.append(score)
            final_candidates.append(temp); final_candidates=sorted(final_candidates,key=lambda i :i[1])
        #print('1st or 2nd of final_abstract_candidate', final_candidates[0], final_candidates[1])
        final_abstract= final_candidates[0][0]
        return final_abstract
        
            
def measure_name_local_info(group):
    num_person= len(finding_person_name(group))
    num_loc = len(finding_loc_name(group))
    total_num= num_person+num_loc
    return total_num




def measure_area(group):
    area=0
    for span in group:
        span_x = span['bbox'][2]-span['bbox'][0]
        span_y = span['bbox'][3]-span['bbox'][1]
        span_area= span_x * span_y ; area = area + span_area 
    return area 

def author_ext(group_list, title_group):
    """
    # criterion
    # 1. order ( sequence, page)
    # 2. person's names scoring 
    """
    title_index = group_list.index(title_group) # title never be author 
    author_candidates=[]
    for i,group in enumerate(group_list):
        temp=[]; total_word_num=0; word_count=0
        if int(group[0]['page']) > 2:
            continue
        for span in group:
            text=span['text']; r_text=repr(text)[2:-1]
            temp1=re.sub(r'[\\]u[a-zA-Z0-9]{4}'," ",r_text);temp2=re.sub(r'[\\]x[a-zA-Z0-9]{2}'," ",temp1)
            if len(temp2) > 0 and count_alpha(temp2) >5 :
                tok=temp2.split(" "); num_tok=len(tok);
                #total_word_num=total_word_num+num_tok
                #print(temp2)
                for i in range(num_tok):
                    if tok[i].isalpha() == False:
                        continue
                    total_word_num= total_word_num +1
                    result=wordnet.synsets(tok[i])
                    #print(tok[i],result) 
                    if len(result) > 0:
                        #print("count up")
                        word_count= word_count+1
        if total_word_num > 1:
            temp.append(group);score=(float(word_count)/float(total_word_num))
            #print('original_score:',score, (int(group[0]['page']))*1.0, int(i)*0.03)
            score=score+(int(group[0]['page'])*1.0)+int(i)*0.01
            temp.append(score)
            #print(temp)
            author_candidates.append(temp)
    author_candidates=sorted(author_candidates,key=lambda i :i[1]) 
    most_possible_author = author_candidates[0][0]
    return most_possible_author    

def count_alpha(string):
    count=0
    for a in string:
        if (a.isalpha())==True:
            count+=1
    return count

def title_ext(group_list):  # this ordered_mspa has page and its grouped order
    """
    #Criteria
    #    1. bigger font size
    #    2. order(sequence, page)
    #    3. word length (not so small and not so big)
    #    4. not in journal list 
    """
    new_group_info =[]
    for group in group_list:
        #print(group) # > group_list_08
        largest_size_span= sorted(group,key=lambda i :i['size'])[-1]
        largest_size= largest_size_span['size']
        new_group_info.append(largest_size)

    #print('new_group_info',new_group_info)
    top_10_idx = np.argsort(new_group_info)[-10:]
    #print('top_10_idx',top_10_idx)
    title_candidate=[]
    for i in range(9,-1,-1):
       title_candidate.append(group_list[top_10_idx[i]])
    #print('title_candidate')
    #for group in title_candidate:
    #    print(group)
    resort_title_candidate=[]
    for group in title_candidate:
        if int(group[0]['page']) > 2:
            #print('Code1', group)
            continue 
        if text_length_check(group)==False:
        # length of title < 20 (two words) > 500( paragraph) ==> should be excluded 
            #print('Code2', group)
            continue
        if journal_list_check(group)== True:
            #print('Code3', group)
            continue
        resort_title_candidate.append(group)
        #print("Added to title group:",group)
    # later, other sicence journal name should be added : ex) Sicence china ..and so on...
    # if font sizes are mixed, it can be from noise data but can be special character.. :ex) file_num:7
    #print('resort_title_candidates', resort_title_candidate)
    if len(resort_title_candidate) ==0 :
        final_title=title_candidate[-1]
    else:
        final_title= resort_title_candidate[0] # Choose the earlier index in candidate
    return final_title
    
def finding_person_name(group): # if there is person's name, return index of first and last
    nlp=spacy.load("en_core_web_sm")
    person_span_index_list=[]
    for i,span in enumerate(group):
        text=span['text']; r_text=repr(text)[2:-1]; temp1=re.sub(r'[\\]u[a-zA-Z0-9]{4}'," ",r_text)
        temp2= re.sub(r'[\\]x[a-zA-Z0-9]{2}'," ",temp1)
        #print(temp2)
        if len(temp2) == 0 or count_alpha(temp2) < 3:
            continue
        temp3 = unicode(temp2,"utf-8")
        doc=nlp(temp3)
        ents_list= [ent.label_ for ent in doc.ents]
        text_list= [ent.text for ent in doc.ents]
        ### 
        for ent in ents_list :
            if ent==u'PERSON':
                #print("PERSON matched",span)
                person_span_index_list.append(i)
    return person_span_index_list


def finding_loc_name(group): # if there is location or organization, return index of first and last
    nlp=spacy.load("en_core_web_sm")
    loc_span_index_list=[]
    for i,span in enumerate(group):
        text=span['text']; r_text=repr(text)[2:-1]; temp1=re.sub(r'[\\]u[a-zA-Z0-9]{4}'," ",r_text)
        temp2= re.sub(r'[\\]x[a-zA-Z0-9]{2}'," ",temp1)
        if len(temp2) == 0 or count_alpha(temp2) < 3:
            continue
        temp3=unicode(temp2,"utf-8")
        doc=nlp(temp3)
        ents_list= [ent.label_ for ent in doc.ents]
        text_list= [ent.text for ent in doc.ents]
        ### 
        for ent in ents_list :
            if ent==u'GPE' or ent==u'ORG':
                loc_span_index_list.append(i)
    return loc_span_index_list


def text_length_check(group):
    text_count=0
    for span in group:
        text_size=len(span['text'])
        text_count= text_count + text_size 
    
    if text_count < 20 or text_count > 500 :
        return False
    else:
        return True


def journal_list_check(group):
    group_text_list=[]
    for span in group:
        text= (span['text'])
        str_text=unicodedata.normalize('NFKD',text).encode('ascii','ignore')
        group_text_list.append(str_text)
    joined_text=" ".join(group_text_list)
    for journal_name in Journal_list:
        total_length=len(joined_text)
        error=nltk.edit_distance(joined_text.lower(),journal_name.lower())
        if float(error)/float(total_length) < 0.1:
            #print("This is journal name", journal_name)
            #print("this is text", joined_text)
            #print('total_length:',total_length, 'error:',error, 'error/total_length:', float(error)/float(total_length))
            return True
        else:
            continue
    return False
        
    



def page_to_group(page_group_list):
    new_list=[]
    for page in page_group_list:
        for group in page:
            new_list.append(group)
    return new_list







# This is list of materials science journals : This data came from Wiki search 
Journal_list= ['ACS Applied Materials & Interfaces',
               'Acta Crystallographica',
               'Acta Materialia',
               'Acta Metallurgica',
               'Scripta Materialia',
               'Advanced Composite Materials',
               'Advanced Materials',
               'Advanced Energy Materials',
               'Advanced Engineering Materials',
               'Adcaned Optical Materials',
               'Beilstein Journal of Nanotechnology',
               'Bulletin of Materials Science',
               'Chemistry of Materials',
               'Computational Materials Science',
               'Crystal Growth & Design',
               'Journal of Applied Crystallography',
               'Journal of colloid and Interface Science',
               'Journal of the European Ceramic Society',
               'Journal of Materials Chemistry A',
               'Materials Chemistry A',
               'Journal of Materials Chemistry B',
               'Materials Chemistry B',
               'Journal of Materials Chemistry C',
               'Materials Chemistry C',
               'Journal of Materials Research and Technology',
               'Journal of Materials Science',
               'Journal of Materials Science Letters',
               'Journal of Materials Science: Materials in Electronics',
               'Journal of Materials Science: Materials in Medicine',
               'Journal of Physical Chemistry B',
               'Materials',
               'Materials & Design',
               'Materials and Structures',
               'Materials Research Bulletin',
               'Materials Research Letters',
               'Materialsi Science and Engineering',
               'Materialsi Science and Engineering-A',
               'Materialsi Science and Engineering-B',
               'Materialsi Science and Engineering-C',
               'Materialsi Science and Engineering-R',
               'Materials Today',
               'Matallurgical and Materials Transactions',
               'Modelling and Simulation Materials Science and Engineering',
               'MRS Bulletin',
               'Physical Review B',
               'Physical Review Materials',
               'Progress in Materials Science',
               'Science and Technology of Advanced Materials',
               'Structural and Multidisciplinary Optimization', # wiki list 
               'Journal of Alloys and Compounds', # added 
               'Microporous and Mesoporous Materials', # added
               'Solar Energy Materials & Solar Cells', # added
               'Chemical Engineering Journal', # added
# china journal list in english
'Acta Biochimica et Biophysica Sinica',
'Acta Geochimica',
'Acta Geologica Sinica',
'Acta Mathematica Scientia',
'Acta Mathematica Sinica. English Series',
'Acta Mathematicae Applicatae Sinica',
'Acta Mechanica Sinica',
'Acta Mechanica Solida Sinica',
'Acta Metallurgica Sinica(English Letters)',
'Acta Oceanologica Sinica',
'Acta Pharmaceutica Sinica B',
'Acta Pharmacologica Sinica',
'Advances in Atmospheric Sciences',
'Advances in Climate Change Research',
'Advances in Manufacturing',
'Algebra Colloquium',
'Analysis in Theory and Applications',
'Applied Geophysics',
'Applied Mathematics and Mechanics',
'Applied Mathematics. Series B, A Journal of Chinese Universities',
'Asian Herpetological Research',
'Asian Journal of Andrology',
'Atmospheric and Oceanic Science Letters',
'Avian Research',
'Biomedical and Environmental Sciences',
'Biophysics Reports',
'Bone Research',
'Building Simulation',
'Cancer Biology and Medicine',
'Cell Research',
'Cellular & Molecular Immunology',
'Chemical Research in Chinese Universities',
'China City Planning Review',
'China Communications',
'China Ocean Engineering',
'Chinese Annals of Mathematics. Series B',
'Chinese Chemical Letters',
'Chinese Geographical Science',
'Chinese Journal of Acoustics',
'Chinese Journal of Aeronautics',
'Chinese Journal of Cancer',
'Chinese Journal of Cancer Research',
'Chinese Journal of Chemical Engineering',
'Chinese Journal of Chemical Physics',
'Chinese Journal of Chemistry',
'Chinese Journal of Electrical Engineering',
'Chinese Journal of Electronics',
'Chinese Journal of Integrative Medicine',
'Chinese Journal of Mechanical Engineering',
'Chinese Journal of Natural Medicines',
'Chinese Journal of Oceanology and Limnology',
'Chinese Journal of Polymer Science',
'Chinese Journal of Structural Chemistry',
'Chinese Journal of Traumatology',
'Chinese Medical Journal',
'Chinese Medical Sciences Journal',
'Chinese Neurosurgical Journal',
'Chinese Optics Letters',
'Chinese Physics Letters',
'Chinese Physics. B',
'Chinese Physics. C',
'Chronic Diseases and Translational Medicine',
'Communications in Mathematical Research',
'Communications in Theoretical Physics',
'Computational Visual Media',
'Control Theory and Technology',
'Current Zoology',
'Defence Technology',
'Earthquake Engineering and Engineering Vibration',
'Earthquake Research in China',
'Earthquake Science',
'Entomotaxonomia',
'Forest Ecosystems',
'Friction',
'Frontiers in Biology',
'Frontiers in Energy',
'Frontiers of Architectural Research',
'Frontiers of Chemical Science and Engineering',
'Frontiers of Computer Science',
'Frontiers of Earth Science',
'Frontiers of Environmental Science & Engineering',
'Frontiers of Information Technology & Electronic Engineering',
'Frontiers of Materials Science',
'Frontiers of Mathematics in China',
'Frontiers of Mechanical Engineering',
'Frontiers of Medicine',
'Frontiers of Optoelectronics',
'Frontiers of Physics',
'Frontiers of Structural and Civil Engineering',
'Genomics, Proteomics & Bioinformatics',
'Geoscience Frontiers',
'Geo-spatial Information Science',
'Hepatobiliary & Pancreatic Diseases International',
'High Power Laser Science and Engineering',
'IEEE/CAA Journal of Automatica Sinica',
'Insect Science',
'Integrative Zoology',
'International Journal of Automation and Computing',
'International Journal of Disaster Risk Science',
'International Journal of Minerals, Metallurgy and Materials',
'International Journal of Mining Science and Technology',
'International Journal of Oral Science',
'International Soil and Water Conservation Research',
'Journal of Acupuncture and Tuina Science',
'Journal of Advanced Ceramics',
'Journal of Animal Science and Biotechnology',
'Journal of Arid Land',
'Journal of Bionic Engineering',
'Journal of Central South University',
'Journal of Chinese Pharmaceutical Sciences',
'Journal of Computational Mathematics',
'Journal of Computer Science and Technology',
'Journal of Data and Information Science',
'Journal of Earth Science',
'Journal of Electronic Science and Technology',
'Journal of Energy Chemistry',
'Journal of Environmental Sciences',
'Journal of Forestry Research',
'Journal of Genetics and Genomics',
'Journal of Geographical Sciences',
'Journal of Geriatric Cardiology',
'Journal of Hydrodynamics',
'Journal of Integrative Agriculture',
'Journal of Integrative Medicine',
'Journal of Integrative Plant Biology',
'Journal of Iron and Steel Research, International',
'Journal of Marine Science and Application',
'Journal of Materials Science & Technology',
'Journal of Mathematical Research with Applications',
'Journal of Measurement Science and Instrumentation',
'Journal of Meteorological Research',
'Journal of Molecular Cell Biology',
'Journal of Mountain Science',
'Journal of Ocean University of China',
'Journal of Otology',
'Journal of Palaeogeography',
'Journal of Partial Differential Equations',
'Journal of Pharmaceutical Analysis',
'Journal of Plant Ecology',
'Journal of Rare Earths',
'Journal of Resources and Ecology',
'Journal of Rock Mechanics and Geotechnical Engineering',
'Journal of Semiconductors',
'Journal of Systematics and Evolution',
'Journal of Systems Engineering and Electronics',
'Journal of Systems Science and Complexity',
'Journal of Systems Science and Information',
'Journal of Systems Science and Systems Engineering',
'Journal of the Operations Research Society of China',
'Journal of Thermal Science',
'Journal of Traditional Chinese Medicine',
'Journal of Zhejiang University. Science A, Applied Physics & Engineering',
'Journal of Zhejiang University. Science B, Biomedicine & Biotechnology',
'Landscape Architecture Frontiers',
'Light:Science & Applications',
'Microsystems & Nanoengineering',
'Molecular Plant',
'Nano Research',
'National Science Review',
'Neural Regeneration Research',
'Neuroscience Bulletin',
'Nuclear Science and Techniques',
'Numerical Mathematics Theory, Methods and Applications',
'Particuology',
'Pedosphere',
'Petroleum Science',
'Photonic Sensors',
'Plant Diversity',
'Plasma Science & Technology',
'Progress in Natural Science: Materials International',
'Protein & Cell',
'Quantitative Biology',
'Rare Metals',
'Reproductive and Developmental Medicine',
'Research in Astronomy and Astrophysics',
'Rice Science',
'Science Bulletin',
'Science China. Chemistry',
'Science China. Earth Sciences',
'Science China. Information Sciences',
'Science China. Life Sciences',
'Science China. Materials',
'Science China. Mathematics',
'Science China. Physics, Mechanics & Astronomy',
'Science China. Technological Sciences',
'Sciences in Cold and Arid Regions',
'Shanghai Archives of Psychiatry',
'The Crop Journal',
'The Journal of Biomedical Research',
'The Journal of China Universities of Posts and Telecommunications',
'Theoretical and Applied Mechanics Letters',
'Transactions of Nanjing University of Aeronautics and Astronautics',
'Transactions of Nonferrous Metals Society of China',
'Tsinghua Science and Technology',
'Virologica Sinica',
'Water Science and Engineering',
'World Journal of Acupuncture-Moxibustion',
'World Journal of Pediatrics',
'Wuhan University Journal of Natural Sciences',
'Zoological Research',
'Zoological Systematics',
# manually added Journal list
'Materials Chemistry C',
'Materials and Design',
'Materials Science in Semiconductor Processing',
'Biosensors and Bioelectronics',
'Materials Chemistry A',
'Advanced Powder Technology',
'Applied Surface Science',
'Catalysts',
'Bioorganic & Medicinal Chemistry',
'Chemical Physics Letters',
'Bioorganic & Medicinal Chemistry Letters'
]
