


def daughter_span_generator(span_list):
    #Splitting a span block into many points.  
    daughter_span_list=[] # new span_list with only daughter spans. In daughter span, text value is not included
    span_index_dict={}    # span_index_dict[mother_span_index] = child_span_index_list 
    daughter_index_start = 0 
    ind =daughter_index_start 
    for i in range(len(span_list)):
        mother_span= span_list[i] 
        daughter_spans, index_list = generating_point_span(mother_span,ind)
        span_index_dict[i+1]=index_list # match mother's index and daughter's index list
        ind = index_list[-1]
        daughter_span_list += daughter_spans
        # - span_index_dict  : matching table between mother span and daughter span(points)
        # - span_list : only has mother span information(original span list + indexing)
        # - daughter_span_list : only has all daughter spans(points) without context information 
    #for i in range(len(daughter_span_list)):
    #    print(daughter_span_list[i])
    return span_index_dict, daughter_span_list




def generating_point_span(span, start_index_num):
    # This 'generating_point_span' split mother span 'bbox' into many daughter span 'point'
    # For later grouping, this function assumes the number of mother span doesn't exceed 100000
    # This function return two variables: the points coordinate of daughter span, and its index
    mother_span=span; index_num=start_index_num;
    m_index = mother_span['index']   # mother span index 
    m_bbox = mother_span['bbox']  # mother span bbox 
    # left_top_x = m_bbox[0]; left_top_y = m_bbox[1]; right_bottom_x =m_bbox[2], right_bottom_y=m_bbox[3]
    x_coordinate_list = point_extractor(m_bbox[0],m_bbox[2])
    y_coordinate_list = point_extractor(m_bbox[1],m_bbox[3])
    combi_list = return_point_combi(x_coordinate_list, y_coordinate_list)
    #print(combi_list)
    daughter_span_dict = {}
    daughter_span_index_list=[]
    for i in range(len(combi_list)):
        index = index_num+i+1
        daughter_span_dict[i]={};
        x_coordinate = float("{0:.2f}".format(combi_list[i][0])); daughter_span_dict[i]['x_val']= x_coordinate;
        y_coordinate = float("{0:.2f}".format(combi_list[i][1])); daughter_span_dict[i]['y_val']= y_coordinate;
        font_size = float("{0:1f}".format(mother_span['size'])); daughter_span_dict[i]['size']= font_size;
        page_num = mother_span['page']; daughter_span_dict[i]['page']= page_num;
        font_type = mother_span['font']; daughter_span_dict[i]['font']=font_type;
        daughter_span_dict[i]['index']=index
        daughter_span_index_list.append(index)
   #return (daughter_span_dict, daughter_index_list)
    #print(daughter_span_dict)
    daughter_span_list= convert_dict_to_list(daughter_span_dict)
    return daughter_span_list, daughter_span_index_list


def point_extractor(start,end):
    interval=end-start; step=0.2# this step is user-defined value (one character usuall takes 4.4 by 9.3)
    point_list = []; point_list.append(start);
    STATUS= True
    while STATUS :
        if start+step < end :
            point_list.append(start+step)
            STATUS= True
            start = start+step
        else:
            STATUS= False
            point_list.append(end)
    return point_list


def return_point_combi(x_list,y_list):
    # return boundary point of bbox using x coordinate point list and y coordinate point list
    combi_list = [] 
    for x_element in x_list:
        temp_low=[]; temp_high=[];
        temp_low.append(x_element) # all x element with the smallest of y element
        temp_low.append(y_list[0]) 
        temp_high.append(x_element) # all x element with the highest of y element
        temp_high.append(y_list[-1]) 
        combi_list.append(temp_low)
        combi_list.append(temp_high)
    for y_element in y_list:
        temp_low=[]; temp_high=[];
        temp_low.append(x_list[0])
        temp_low.append(y_element)
        temp_high.append(x_list[-1])
        temp_high.append(y_element)
        combi_list.append(temp_low)
        combi_list.append(temp_high)

    return combi_list

def convert_dict_to_list(dict_a):
    return_list=[]
    for i in range(len(dict_a.keys())):
        #print(i); print(i+1); print(dict_a[int(i)+1]);
        return_list.append(dict_a[i])
    return return_list
    
            
