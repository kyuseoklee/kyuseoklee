# -*- coding: utf- 8 - *-
from copy import deepcopy
import re
import os
import json
import nltk

def compare_two(sub_title_list, matching_dict, lines, number_of_file): # compare ground truth (stl,md) and lines 
    os.system('mkdir ./BA_output/%s' % number_of_file)
    f=open('/home/coollooter/metadata_extractor/BA_output/%s/%s.txt'% (number_of_file,number_of_file), "w+")
    check_line_dict={}
    index_line_dict={}
    sub_title_list_temp=is_value_none(sub_title_list,matching_dict) #check if value =="" and removed
    check_subtitle=is_value_none(sub_title_list,matching_dict) #check if value =="" and removed
    for i in range(len(lines)):
        f.writelines(str(lines[i])+"\n"); line_index=lines[i]['index'];
        check_line=line_modi(lines[i]) # line[i] -> no 'u\2131 no space : sequence of char
        MATCH_STATUS= False
        if len(check_line) > 10 : 
            for key in check_subtitle:
                check_text=origin_text_modi(matching_dict[key]) # text-> no'u'212 no space: sequence of char
                if check_line[-1]=='-':
                    check_line=check_line[:-1]
                m_index =check_text.find(check_line)
                #print(new_line_temp,m_index,temp)
                if m_index > -1:
                    MATCH_STATUS=True
                    if key not in check_line_dict.keys(): # create new key value in new_line_dict
                        check_line_dict[key]=check_line
                        index_line_dict[key]=[];index_line_dict[key].append(line_index)
                        check_subtitle=if_finished_fill(check_line_dict[key],check_text,check_subtitle,key)
                        break
                    else:
                        if check_line_dict[key][-1] =='-':
                            check_line_dict[key]=check_line_dict[key][:-1]+check_line
                            index_line_dict[key].append(line_index)
                            check_subtitle=if_finished_fill(check_line_dict[key],check_text,check_subtitle,key)
                            break
                        else:
                            check_line_dict[key]=check_line_dict[key]+check_line
                            index_line_dict[key].append(line_index)
                            check_subtitle=if_finished_fill(check_line_dict[key],check_text,check_subtitle,key)
                            break
                    #print("KEY:", key)
                    #result=if_finished_fill(check_line_dict[key],check_text,check_subtitle)
                    #if result ==0:
                        #check_subtitle.remove(key)
                        #print(new_subtitle, key)
            if MATCH_STATUS == False : 
                #print (check_line,'not exist in dict')
                a=1
        else: # if len(new_line) <= 10 
            a=1
            #print(check_line, 'len(line) is <= 10 manual check is needed')
    f.close()   
        #print(key)
    #print(new_line_dict.keys())

    f=open('/home/coollooter/metadata_extractor/BA_output/%s/%s_index.txt'% (number_of_file,number_of_file), "w+")
    f.writelines(str(sub_title_list_temp)+"\n") 

    for i in range(len(sub_title_list_temp)):
        key=sub_title_list_temp[i]; 
        #print("compare check vs matching")
        #print(check_line_dict.keys())
        #print(matching_dict.keys())
        result=(result_fill_check(check_line_dict[key],origin_text_modi(matching_dict[key])))
        print(key, result)
        f.writelines(str(key+" "+str(result))+"\n") 
        f.writelines(str(index_line_dict[key])+"\n")
        if result != 0:
            f.writelines("Above key result is not 0. Compare Origin vs Line\n")
            f.writelines(str(origin_text_modi(matching_dict[key])+"\n"))
            f.writelines(str(check_line_dict[key])+"\n")
            f.writelines(str(matching_dict[key]+"\n"))
            for j in range(len(index_line_dict[key])):
                index = index_line_dict[key][j]
                #print(index)
                f.writelines(repr(lines[index-1]['text'])[2:-1])
            f.writelines("\n")
    return None


def result_fill_check(origin_text,new_text): # check if two text are exactly the same, return result 
    result=nltk.edit_distance(origin_text,new_text)
    return result

def if_finished_fill(origin_text, new_text,check_subtitle,key):# check if two text are exactly the same, remove subtitle
    #print("Origin vs NEW")
    origin=origin_text; new=new_text;
    #print("Origin")
    #print(origin)
    #print("new")
    #print(new)
    #print("Above length",len(origin),len(new))
    result=-1
    if len(origin)==len(new):
        result=nltk.edit_distance(origin,new)
        #print(result)
    #else:
    #    result=-1
    if result==0:
        check_subtitle.remove(key)
    return check_subtitle


def is_value_none(sub_title_list,dictionary):
    # remove key & value if value of key is None or ""
    new_subtitle= deepcopy(sub_title_list)
    for key in dictionary.keys():
        if len(dictionary[key])==0:
            new_subtitle.remove(key)
    return new_subtitle



def line_modi(line):
    # a line is modified for later comparision with GT text
    # 1. literal text is extracted from unicode format[step1] 
    # 2. \\u0A21  or \xef type removed[step2]
    # 3. Any space is removed -> sequence of character[ste3] 
    #(removed)# 4. if last character =='-', remove
    line_text=line['text']; r_line_text=repr(line_text)[2:-1];#step1
    #print(r_line_text)
    # 'u/'asbsd \u03b4 is extracted or can be replaced with \\
    temp1=re.sub(r'[\\]u[a-zA-Z0-9]{4}',"",r_line_text) #step2
    temp2=re.sub(r'[\\]x[a-zA-Z0-9]{2}',"",temp1)
    new_line=re.sub(" ","",temp2)#step3
    #print(new_line)
    #if new_line[-1]=='-':
    #    new_line=new_line[:-2] # step4
    return new_line


def origin_text_modi(text):
    # a text is modifed for later comparision 
    # 1 remove \u0112, \xef
    # 2 remove any space
    temp1 = re.sub(r'[\\]u[a-zA-Z0-9]{4}',"",text) # step1
    temp2 = re.sub(r'[\\]x[a-zA-Z0-9]{2}',"",temp1) # step1
    new_text=re.sub(" ","",temp2) # step2
    return new_text


