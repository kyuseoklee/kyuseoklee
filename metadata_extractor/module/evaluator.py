
import nltk

def eval(product,ground_t_file):
    DB_title=product[0]; DB_author=product[1]; DB_abstract=product[2]; DB_pre_text=product[3]; DB_sub_body=product[4]
    DB_title=DB_extract(DB_title); 
    DB_author=DB_extract(DB_author);
    DB_abstract=DB_extract(DB_abstract);
    DB_pre_text= DB_para_extract(DB_pre_text)
    DB_subtitle, DB_body= DB_body_extract(DB_sub_body)

    GT_title, GT_author, GT_author_info, GT_abstract, GT_pre_text, GT_subtitle, GT_body= GT_extract(ground_t_file)

    title_result= similarity_main_measure(DB_title,GT_title)
    #print('DB title vs GT title', DB_title, GT_title)
    author_result = similarity_main_measure(DB_author,GT_author) 
    #print('DB author vs GT author', DB_author,GT_author)
    abstract_result= similarity_main_measure(DB_abstract,GT_abstract) 
    #print('DB abstract vs GT abstract', DB_abstract, GT_abstract)
    body_result= similarity_body_measure(DB_pre_text,GT_pre_text,DB_subtitle,GT_subtitle,DB_body,GT_body)
    #print('DB body vs GT body', DB_body, GT_body)
    
    
    return title_result, author_result, abstract_result, body_result
    


def similarity_body_measure(DB_pre_text,GT_pre_text,DB_subtitle,GT_subtitle,DB_body,GT_body):
    total_score=0
    gt_subtitle_num=len(GT_subtitle); # whole body number 
    if len(GT_pre_text) !=0:
        total_num=gt_subtitle_num+1
        pre_text_result=similarity_main_measure(DB_pre_text,GT_pre_text)
        total_score=total_score + (pre_text_result/total_num)
    else:
        total_num=gt_subtitle_num
        pre_text_result=0
    for i,subtitle in enumerate(GT_subtitle): 
        gt_sub = subtitle ; 
        if gt_sub in DB_subtitle:
            db_sub_index = DB_subtitle.index(gt_sub)
            result=similarity_main_measure(GT_body[i],DB_body[db_sub_index])
            #print('body_part:', GT_body[i], DB_body[db_sub_index], result)
            total_score= total_score + (result/total_num)
        else: 
            a=1
    return total_score
    

def similarity_main_measure(alist, blist):
    A=len(alist); B=len(blist)
    if B > A :
        temp=alist; alist=blist; blist=temp
    A=len(alist)
    result=nltk.edit_distance(alist, blist)
    #print('inner result', result)
    return 100-(float(result)/float(A)*100)

def DB_body_extract(DB_sub_body):
    subtitle_list=[]; body_list=[]
    for i,sub_combi in enumerate(DB_sub_body):
        subtitle=sub_combi[0]; paragraph= sub_combi[1]
        temp_sub=DB_extract(subtitle); 
        temp_para=DB_para_extract(paragraph);
        subtitle_list.append(temp_sub); body_list.append(temp_para)
    return subtitle_list, body_list
    
def DB_para_extract(paragraph):
    res=[]
    for group in paragraph:
        for span in group:
            index= span['index']; res.append(index)
    return res
        
def DB_extract(DB_text): # for title, author, abstract, pre_text # for one group 
    #print(DB_text)
    res=[]
    if DB_text==None:
        res=[]
    else:
        for span in DB_text:
            index = span['index'] ; res.append(index)
    return res


def GT_extract(ground_t_file):
    author_info_status=False;pre_text_status=False
    with open(ground_t_file) as f :
        lines=f.read().splitlines()
        for i in range(len(lines)):
            if i == 0 :
                continue
            tok=lines[i].split("[")
            if len(tok[0])!=0: # when it has title ex) abstract = [ xxx , xx]
                a=1
                if tok[0].find('title')==0  : 
                    temp_title_index=tok[1]; title_index=temp_title_index.split("]")[0]
                    title=paragraph_index_rephrase(title_index)
                if tok[0].find('author=') > -1 :
                    temp_author_index=tok[1]; author_index=temp_author_index.split("]")[0]
                    #print('author_index',author_index)
                    author=paragraph_index_rephrase(author_index)
                if tok[0].find('author_info') > -1 :
                    temp_author_info_index=tok[1]; author_info_index=temp_author_info_index.split("]")[0]
                    author_info=paragraph_index_rephrase(author_info_index)
                    author_info_status=True
                if tok[0].find('abstract') > -1:
                    temp_abstract_info_index=tok[1]; abstract_info_index=temp_abstract_info_index.split("]")[0]
                    abstract=paragraph_index_rephrase(abstract_info_index)
                if tok[0].find('subtitle_list')> -1:
                    temp_subtitle_list_index=tok[1]; subtitle_list_index= temp_subtitle_list_index.split("]")[0]
                    subtitle_line_index=i; subtitle_list=subtitle_index_rephrase(subtitle_list_index)
                if tok[0].find('pre_text') > -1:
                    temp_pre_text_index=tok[1]; pre_text_index=temp_pre_text_index.split("]")[0]
                    pre_text= paragraph_index_rephrase(pre_text_index)
                    pre_text_status=True
        subtitle=subtitle_list_index.split(",")
        num_subtitle=len(subtitle)
        body=[]
        for i in range(num_subtitle):
           temp= lines[i+1+subtitle_line_index]; temp=temp[1:-1]
           para = paragraph_index_rephrase(temp)  
           body.append(para)
           
        #print('title',title)
        #print('author', author)
        #print('author_info', author_info)
        #print('abstract', abstract)
        if author_info_status==True:
            a=1
        else:
            author_info=[]
        if pre_text_status ==True :
            a=1
        else:
            pre_text=[]
        #print('subtitle_list', subtitle_list)
        #for i in range(len(body)):
        #    print('body',body[i])
    return title, author, author_info, abstract, pre_text, subtitle_list, body 

def subtitle_index_rephrase(subtitle_index): 
    res=[]; tok=subtitle_index.split(","); num_tok=len(tok)
    for i in range(num_tok):
        index_list=[]
        if tok[i].find(':') > -1:
            index=tok[i].split(':'); left_index=index[0]; right_index=index[1]
            for j in range(int(left_index),(int(right_index)+1)):
                index_list.append(int(j))
        else:
            index_list=[int(tok[i])]
        res.append(index_list)
    return res



def paragraph_index_rephrase(pseudo_index): # pseudo_index 69:117,130:140
    tok=pseudo_index.split(","); num_tok= len(tok)
    res=[];index_list=[]
    for i in range(num_tok):
        #print(tok[i])
        index_list=[]
        if tok[i].find(':') > -1:
            index=tok[i].split(':'); left_index=index[0]; right_index=index[1]
            for j in range(int(left_index),(int(right_index)+1)):
                index_list.append(j)
        else:
            index_list=[int(tok[i])]
        res=res+index_list
    return res



                
              
                
